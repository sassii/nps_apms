class CreateTMitumoris < ActiveRecord::Migration[5.2]
  def change
    create_table :t_mitumoris do |t|
      t.string :mitumu_no
      t.integer :mitum_eda
      t.date :mitumbi
      t.string :mitumtanto
      t.string :kokyakucd
      t.string :kokyaku
      t.string :kokyaku2
      t.string :kokyaku_tanto
      t.text :mitummei
      t.string :zuban
      t.string :hinmei
      t.integer :suryo
      t.integer :tanka
      t.integer :kin
      t.integer :soryo
      t.text :biko
      t.text :tekiyo
      t.integer :s_tanka
      t.integer :z_tanka
      t.integer :k_tanaka
      t.integer :gokei
      t.integer :status
      t.date :kanryobi
      t.text :riyu
      t.string :gaityucd1
      t.integer :gaityusuryo1
      t.integer :gaityutanka1
      t.integer :uritanka1
      t.string :gaityucd2
      t.integer :gaityusuryo2
      t.integer :gaityutanka2
      t.integer :uritanka2
      t.string :gaityucd3
      t.integer :gaityusuryo3
      t.integer :gaityutanka3
      t.integer :uritanka3
      t.string :gaityucd4
      t.integer :gaityusuryo4
      t.integer :gaityutanka4
      t.integer :uritanka4
      t.string :gaityucd5
      t.integer :gaityusuryo5
      t.integer :gaityutanka5
      t.integer :uritanka5
      t.text :syoken
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
