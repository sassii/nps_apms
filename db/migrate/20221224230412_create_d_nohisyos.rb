class CreateDNohisyos < ActiveRecord::Migration[5.2]
  def change
    create_table :d_nohisyos do |t|
      t.string :uriageno
      t.integer :uriageeda
      t.date :uriagebi
      t.string :kokyakucd
      t.string :kokyaku
      t.string :yubin
      t.string :jyusyo1
      t.string :jyusyo2
      t.string :tanto
      t.string :zuban
      t.string :hinmei
      t.integer :suryo
      t.string :tani
      t.integer :tanka
      t.integer :kin
      t.text :biko
      t.string :jyutyuno
      t.integer :jyutyueda
      t.text :tekiyo

      t.timestamps
    end
  end
end
