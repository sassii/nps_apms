class CreateWorkItems < ActiveRecord::Migration[7.1]
  def change
    create_table :work_items do |t|
      t.integer :wprocess_id
      t.string :work_item_name
      t.integer :quantity
      t.integer :employee_id
      t.integer :machine_id
      t.decimal :planned_hours
      t.decimal :actual_hours
      t.date :actual_start_date
      t.date :actual_end_date
      t.time :actual_start_time
      t.time :actual_end_time

      t.timestamps
    end
  end
end
