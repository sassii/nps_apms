class CreateOrderHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :order_histories do |t|
      t.integer :ulist_id
      t.date :o_date
      t.string :order_no
      t.string :hinmei
      t.integer :total

      t.timestamps
    end
  end
end
