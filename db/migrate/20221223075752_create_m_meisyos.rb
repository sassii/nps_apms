class CreateMMeisyos < ActiveRecord::Migration[5.2]
  def change
    create_table :m_meisyos do |t|
      t.string :sikibetu
      t.string :meisyo
      t.integer :sort

      t.timestamps
    end
  end
end
