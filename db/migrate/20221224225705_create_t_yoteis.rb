class CreateTYoteis < ActiveRecord::Migration[5.2]
  def change
    create_table :t_yoteis do |t|
      t.date :nengetu
      t.date :yoteibi
      t.string :kokyakucd
      t.integer :yoteikin
      t.date :nyukinbi
      t.integer :kin
      t.integer :tesuryo
      t.integer :nebiki
      t.integer :zankin

      t.timestamps
    end
  end
end
