class CreateDJsyokais < ActiveRecord::Migration[5.2]
  def change
    create_table :d_jsyokais do |t|
      t.string :jyutyuno
      t.string :kokyaku
      t.string :zuban
      t.string :hinmei
      t.integer :suryo
      t.integer :tanka
      t.integer :kin
      t.date :noki
      t.date :kanryobi
      t.text :biko
      t.integer :gai_kbn
      t.text :oboegaki

      t.timestamps
    end
  end
end
