class CreateWorkProgresses < ActiveRecord::Migration[7.1]
  def change
    create_table :work_progresses do |t|
      t.integer :work_item_id
      t.integer :status_id
      t.date :progress_date
      t.text :notes

      t.timestamps
    end
  end
end
