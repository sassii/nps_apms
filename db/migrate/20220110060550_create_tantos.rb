class CreateTantos < ActiveRecord::Migration[5.2]
  def change
    create_table :tantos do |t|
      t.string :tanto_cd
      t.string :tanto_name

      t.timestamps
    end
  end
end
