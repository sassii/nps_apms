class CreateMachines < ActiveRecord::Migration[7.1]
  def change
    create_table :machines do |t|
      t.string :machine_name
      t.string :naiyo
      t.string :place

      t.timestamps
    end
  end
end
