class AddAdress2ToOrderHeaders < ActiveRecord::Migration[5.2]
  def change
    add_column :order_headers, :nhn_adress, :text
    add_column :order_headers, :sky_adress, :text
  end
end
