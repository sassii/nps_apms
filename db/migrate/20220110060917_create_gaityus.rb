class CreateGaityus < ActiveRecord::Migration[5.2]
  def change
    create_table :gaityus do |t|
      t.string :gai_cd
      t.string :gai_name
      t.string :gai_kana
      t.string :gai_ryaku
      t.string :gai_naiyo
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
