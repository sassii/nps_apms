class CreateTRirekis < ActiveRecord::Migration[5.2]
  def change
    create_table :t_rirekis do |t|
      t.string :supportno
      t.integer :supporteda
      t.string :dkokyakucd
      t.date :kaisibi
      t.date :kosinbi
      t.date :supportbi
      t.string :tanto
      t.text :naiyo
      t.string :keka
      t.text :biko
      t.integer :status

      t.timestamps
    end
  end
end
