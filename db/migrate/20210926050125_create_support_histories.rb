class CreateSupportHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :support_histories do |t|
      t.integer :ulist_id
      t.date :s_date
      t.string :s_content
      t.string :s_tanto

      t.timestamps
    end
  end
end
