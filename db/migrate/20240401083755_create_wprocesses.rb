class CreateWprocesses < ActiveRecord::Migration[7.1]
  def change
    create_table :wprocesses do |t|
      t.integer :t_order_id
      t.string :process_no
      t.string :gaityucd
      t.string :process_name
      t.date :start_date
      t.date :end_date
      t.date :actual_start_date
      t.date :actual_end_date
      t.date :deadline

      t.timestamps
    end
  end
end
