class CreateTHatyus < ActiveRecord::Migration[5.2]
  def change
    create_table :t_hatyus do |t|
      t.string :hatyuno
      t.integer :hatyueda
      t.date :hatyubi
      t.string :tantocd
      t.string :kokyakucd
      t.string :gaityucd
      t.string :zuban
      t.string :hinmei
      t.string :naiyo
      t.integer :suryo
      t.integer :jtanka
      t.integer :htanka
      t.date :noki
      t.date :gainoki
      t.date :kanryobi
      t.string :jyutyuno
      t.integer :jyutyueda
      t.string :renban
      t.integer :status
      t.integer :tanka_flg
      t.integer :biko
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
