class CreateTSeikyumeisais < ActiveRecord::Migration[5.2]
  def change
    create_table :t_seikyumeisais do |t|
      t.string :kokyakucd
      t.string :kokyaku
      t.date :simenengetu
      t.date :uriagebi
      t.integer :uri_kbn
      t.string :uriageno
      t.integer :uriageeda
      t.string :zuban
      t.string :hinmei
      t.integer :suryo
      t.string :tani
      t.integer :tanka
      t.integer :kin
      t.integer :zei
      t.text :biko
      t.string :jyutyuno
      t.integer :jyutyueda
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
