class CreateOrderHeaders < ActiveRecord::Migration[5.2]
  def change
    create_table :order_headers do |t|
      t.string :order_no
      t.date :order_day
      t.integer :order_kbn
      t.integer :order_dkbn
      t.references :tanto, foreign_key: true
      t.string :order_tantoname
      t.references :tokui, foreign_key: true
      t.string :order_tokuiname
      t.string :order_kojin
      t.string :order_kaisya
      t.string :order_zip
      t.text :order_adress
      t.string :order_tel
      t.string :order_kana
      t.integer :order_kin
      t.integer :order_tax
      t.integer :order_soryo
      t.integer :order_daibiki
      t.integer :order_sogaku
      t.integer :order_furikomi
      t.date :order_furikomibi
      t.text :order_tekiyo
      t.boolean :doc_seikyu
      t.boolean :doc_mitumori
      t.boolean :doc_ryosyu
      t.date :doc_ryosyubi
      t.text :doc_biko
      t.boolean :pac_photo
      t.boolean :pac_henkyaku
      t.boolean :pac_konpo
      t.boolean :pac_konpotanto
      t.text :pac_biko
      t.string :snd_way
      t.text :snd_biko
      t.text :ssj_biko1
      t.text :ssjbiko2
      t.text :kis_biko
      t.string :nhn_kojin
      t.string :nhn_kaisya
      t.string :nhn_zip
      t.string :nhn_tel
      t.string :nhn_kana
      t.string :order_addname
      t.string :sky_kojin
      t.string :sky_kaisya
      t.string :sky_zip
      t.string :sky_kana
      t.string :sky_tel
      t.integer :order_yuko

      t.timestamps
    end
  end
end
