class CreateMHinbans < ActiveRecord::Migration[5.2]
  def change
    create_table :m_hinbans do |t|
      t.string :hinban
      t.integer :kbn
      t.integer :suti
      t.text :biko
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
