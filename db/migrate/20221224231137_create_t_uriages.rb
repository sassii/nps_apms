class CreateTUriages < ActiveRecord::Migration[5.2]
  def change
    create_table :t_uriages do |t|
      t.string :uriageno
      t.integer :uriageeda
      t.date :uriagebi
      t.integer :uri_kbn
      t.string :jyutyuno
      t.integer :jyutyueda
      t.integer :kokyakucd
      t.string :zuban
      t.string :hinmei
      t.integer :suryo
      t.string :tani
      t.integer :tanka
      t.integer :kin
      t.integer :zei
      t.text :biko
      t.integer :sime_kbn
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
