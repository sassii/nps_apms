class CreateTSeikyusyos < ActiveRecord::Migration[5.2]
  def change
    create_table :t_seikyusyos do |t|
      t.string :seikyuno
      t.integer :seikyueda
      t.string :atena1
      t.string :atena2
      t.string :tanto
      t.string :yubin
      t.text :biko1
      t.text :biko2
      t.text :biko3
      t.text :jyusyo1
      t.text :jyusyo2
      t.integer :zenkaiseikyu
      t.integer :nyukin
      t.integer :kurikosi
      t.integer :keijyo
      t.integer :zeigaku
      t.integer :seikyu
      t.integer :seikyugokei
      t.integer :soryo
      t.date :uriagebi
      t.string :uriageno
      t.integer :uriageeda
      t.string :jyutyuno
      t.integer :jyutyueda
      t.string :zuban
      t.string :hinmei
      t.integer :suryo
      t.string :tani
      t.integer :tanka
      t.integer :kin
      t.integer :zei
      t.text :biko
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
