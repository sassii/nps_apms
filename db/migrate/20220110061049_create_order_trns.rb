class CreateOrderTrns < ActiveRecord::Migration[5.2]
  def change
    create_table :order_trns do |t|
      t.references :order_header, foreign_key: true
      t.string :order_no
      t.integer :order_eda
      t.string :trn_hinmoku
      t.string :trn_zuban
      t.string :trn_hinmei
      t.integer :trn_suryo
      t.integer :trn_sakuseisu
      t.integer :trn_tanka
      t.integer :trn_kin
      t.integer :trn_zei
      t.integer :trn_zaihi
      t.integer :trn_prghi
      t.integer :trn_sts
      t.date :trn_noki
      t.date :trn_dnoki
      t.date :trn_kanryobi
      t.date :trn_syukabi
      t.date :trn_furikomibi
      t.date :trn_uriagebi
      t.string :trn_uriageno
      t.integer :trn_uriageeda
      t.text :trn_biko1
      t.text :trn_biko2
      t.text :trn_biko3
      t.integer :trn_maxeda
      t.integer :trn_yuko

      t.timestamps
    end
  end
end
