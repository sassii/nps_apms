class CreateKokyakus < ActiveRecord::Migration[5.2]
  def change
    create_table :kokyakus do |t|
      t.string :kokyakucd
      t.string :kokyaku
      t.string :kana
      t.string :meisyo
      t.string :s_atena
      t.string :s_atena2
      t.string :gyosyu
      t.string :tiiki
      t.integer :simebi
      t.string :jyoken
      t.integer :kaisyu
      t.string :site
      t.string :kai_jyoken1
      t.string :kai_naiyo1
      t.string :tegata
      t.string :kai_jyoken2
      t.string :kai_naiyo2
      t.string :tegata2
      t.integer :tesuryo
      t.string :eigyo_tanto
      t.string :keiri_tanto
      t.string :zipcd
      t.string :jyusyo1
      t.string :jyusyo2
      t.string :telno
      t.string :s_zipcd
      t.string :s_jyusyo1
      t.string :s_jyusyo2
      t.text :s_biko1
      t.text :s_biko2
      t.text :s_biko3
      t.string :tantocd
      t.integer :sendenkbn
      t.integer :yuko_kbn
      t.text :biko
      t.text :seizobiko

      t.timestamps
    end
  end
end
