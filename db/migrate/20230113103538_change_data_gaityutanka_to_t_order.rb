class ChangeDataGaityutankaToTOrder < ActiveRecord::Migration[5.2]
  def change
    change_column :t_orders, :gaityutanka1, 'integer USING CAST(gaityutanka1 AS integer)'
    change_column :t_orders, :gaityutanka2, 'integer USING CAST(gaityutanka2 AS integer)'
    change_column :t_orders, :gaityutanka3, 'integer USING CAST(gaityutanka3 AS integer)'
    change_column :t_orders, :gaityutanka4, 'integer USING CAST(gaityutanka4 AS integer)'
    change_column :t_orders, :gaityutanka5, 'integer USING CAST(gaityutanka5 AS integer)'
    change_column :t_orders, :gaityutanka6, 'integer USING CAST(gaityutanka6 AS integer)'
    change_column :t_orders, :gaityutanka7, 'integer USING CAST(gaityutanka7 AS integer)'
    change_column :t_orders, :gaityutanka8, 'integer USING CAST(gaityutanka8 AS integer)'
    change_column :t_orders, :gaityutanka9, 'integer USING CAST(gaityutanka9 AS integer)'
    change_column :t_orders, :gaityutanka10, 'integer USING CAST(gaityutanka10 AS integer)'
  end
end
