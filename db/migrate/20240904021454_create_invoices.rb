class CreateInvoices < ActiveRecord::Migration[7.1]
  def change
    create_table :invoices do |t|
      t.integer :kokyaku_id
      t.date :issued_date
      t.datetime :sent_at
      t.string :pdf_path

      t.timestamps
    end
  end
end
