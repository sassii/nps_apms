class AddImageToOrderTrns < ActiveRecord::Migration[5.2]
  def change
    add_column :order_trns, :image, :string
  end
end
