class CreateJOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :j_orders do |t|
      t.string :jyutyuno
      t.date :jyutyubi
      t.string :seikyucd
      t.string :kojinmei
      t.string :kaisyamei
      t.string :zipcd
      t.text :jyusyo
      t.string :telno
      t.string :furigana
      t.string :jyusyomeisyo
      t.string :nohin_kojinmei
      t.string :nohin_kaisyamei
      t.string :nohin_zipcd
      t.text :nohin_jyusyo
      t.string :nohin_telno
      t.string :nohin_furigana
      t.integer :tax
      t.integer :soryo
      t.integer :daibiki
      t.integer :sogaku
      t.integer :seikyusyokbn
      t.integer :mitumorisyokbn
      t.integer :ryosyusyokbn
      t.date :ryosyusyohakkobi
      t.string :syoruibiko
      t.string :zenkaisyoruibiko
      t.integer :photokbn
      t.integer :konpokbn
      t.string :konposya
      t.string :zenkaikonposya
      t.integer :henkyakukbn
      t.string :hassohoho
      t.text :hassobiko
      t.integer :zenkaihassobiko
      t.text :sijisyobiko
      t.text :sijisyobiko2
      t.text :tekiyo
      t.integer :furikomikbn
      t.date :hurikomibi
      t.date :noki
      t.string :sijisyochk1
      t.string :sijisyochk2
      t.string :sijisyochk3
      t.string :sijisyochk4
      t.string :sijisyochk5
      t.string :sijisyochk6
      t.string :sijisyochk7
      t.string :sijisyochk8
      t.string :sijisyochk9
      t.string :sijisyochk10
      t.string :daigakucd
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
