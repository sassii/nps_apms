class CreateMSeihins < ActiveRecord::Migration[5.2]
  def change
    create_table :m_seihins do |t|
      t.string :hinban
      t.string :seihinmei
      t.string :seihinmei2
      t.integer :tanka
      t.text :biko
      t.string :yuko_kbn

      t.timestamps
    end
  end
end
