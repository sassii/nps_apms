class CreateTSeikyuhds < ActiveRecord::Migration[5.2]
  def change
    create_table :t_seikyuhds do |t|
      t.string :kokyakucd
      t.date :seikyunengetu
      t.integer :zenkaiseikyu
      t.integer :nyukin
      t.integer :kurikosi
      t.integer :keijyo
      t.integer :zei
      t.integer :seikyu
      t.integer :seikyugokei

      t.timestamps
    end
  end
end
