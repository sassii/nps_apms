class CreateTNyukins < ActiveRecord::Migration[5.2]
  def change
    create_table :t_nyukins do |t|
      t.string :denpyono
      t.string :kokyakucd
      t.date :simenengetu
      t.date :nyukinbi
      t.text :biko
      t.string :kaisyu1
      t.integer :kin1
      t.text :tekiyo1
      t.date :kijitu1
      t.string :kaisyu2
      t.integer :kin2
      t.text :tekiyo2
      t.date :kijitu2
      t.string :kaisyu3
      t.integer :kin3
      t.text :tekiyo3
      t.date :kijitu3
      t.string :kaisyu4
      t.integer :kin4
      t.text :tekiyo4
      t.date :kijitu4
      t.string :kaisyu5
      t.integer :kin5
      t.text :tekiyo5
      t.date :kijitu5
      t.string :kaisyu6
      t.integer :kin6
      t.text :tekiyo6
      t.date :kijitu7
      t.string :kaisyu7
      t.integer :kin7
      t.text :tekiyo7
      t.date :kijitu7
      t.integer :tesuryo
      t.integer :nebiki
      t.integer :gokei
      t.string :yuko_kbn

      t.timestamps
    end
  end
end
