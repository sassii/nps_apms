class CreateNumbers < ActiveRecord::Migration[5.2]
  def change
    create_table :numbers do |t|
      t.string :type
      t.integer :current

      t.timestamps
    end
  end
end
