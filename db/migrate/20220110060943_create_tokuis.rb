class CreateTokuis < ActiveRecord::Migration[5.2]
  def change
    create_table :tokuis do |t|
      t.string :tokui_cd
      t.string :tokui_name
      t.string :tokui_kana
      t.string :tokui_meisyo
      t.string :seikyu_atena1
      t.string :seikyu_atena2
      t.string :tokui_gyosyu
      t.string :tokui_tiiki
      t.integer :tokui_simebi
      t.string :tokui_jyoken
      t.integer :kaisyu_bi
      t.string :kaisyu_site
      t.string :kaisyu_jyiken1
      t.string :kaisyu_naiyo1
      t.string :kaisyu_tegata1
      t.string :kaisyu_jyiken2
      t.string :kaisyu_naiyo2
      t.string :kaisyu_tegata2
      t.integer :kaisyu_tesuryo
      t.string :tokui_tanto
      t.string :tokui_keiri
      t.string :tokui_zip
      t.string :tokui_adress1
      t.string :tokui_adress2
      t.string :tokui_tel
      t.string :seikyu_zip
      t.string :seikyu_adress1
      t.string :seikyu_adress2
      t.text :seikyu_biko1
      t.text :seikyu_biko2
      t.text :seikyu_biko3
      t.string :seikyu_tanto
      t.integer :senden_kbn
      t.integer :yuko_kbn
      t.integer :seizo_kbn

      t.timestamps
    end
  end
end
