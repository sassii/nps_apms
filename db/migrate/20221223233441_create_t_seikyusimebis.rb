class CreateTSeikyusimebis < ActiveRecord::Migration[5.2]
  def change
    create_table :t_seikyusimebis do |t|
      t.string :kokyakucd
      t.date :simenengetu
      t.integer :simebi
      t.integer :keisu
      t.integer :kin
      t.integer :zeiritu
      t.integer :zei
      t.integer :s_kbn

      t.timestamps
    end
  end
end
