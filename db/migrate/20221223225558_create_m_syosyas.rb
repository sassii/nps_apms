class CreateMSyosyas < ActiveRecord::Migration[5.2]
  def change
    create_table :m_syosyas do |t|
      t.string :kokyakucd
      t.string :syosyamei
      t.string :sijiyochk1
      t.string :sijiyochk2
      t.string :sijiyochk3
      t.string :sijiyochk4
      t.string :sijiyochk5
      t.string :sijiyochk6
      t.string :sijiyochk7
      t.string :sijiyochk8
      t.string :sijiyochk9
      t.string :sijiyochk10
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
