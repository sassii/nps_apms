class CreateKakakus < ActiveRecord::Migration[5.2]
  def change
    create_table :kakakus do |t|
      t.string :scode
      t.string :hinban
      t.integer :kakaku
      t.text :biko

      t.timestamps
    end
  end
end
