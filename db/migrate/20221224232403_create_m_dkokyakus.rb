class CreateMDkokyakus < ActiveRecord::Migration[5.2]
  def change
    create_table :m_dkokyakus do |t|
      t.string :dkokyakucd
      t.string :name
      t.string :daigaku
      t.string :gakubu
      t.string :gaka
      t.string :kenkyusyo
      t.string :yubin
      t.text :jyusyo
      t.string :keisyo
      t.string :yakusyoku
      t.string :telno
      t.string :mail
      t.integer :kaisu
      t.integer :kin
      t.date :jyutyubi
      t.date :mitumoribi
      t.string :status

      t.timestamps
    end
  end
end
