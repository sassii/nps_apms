class CreateMSetteis < ActiveRecord::Migration[5.2]
  def change
    create_table :m_setteis do |t|
      t.string :kbn
      t.string :naiyo1
      t.integer :naiyo2

      t.timestamps
    end
  end
end
