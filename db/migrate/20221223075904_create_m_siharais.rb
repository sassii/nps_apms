class CreateMSiharais < ActiveRecord::Migration[5.2]
  def change
    create_table :m_siharais do |t|
      t.string :siharaicd
      t.string :siharaimei
      t.string :kana
      t.integer :hikikbn
      t.text :biko
      t.string :gaityucd
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
