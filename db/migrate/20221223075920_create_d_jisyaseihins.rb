class CreateDJisyaseihins < ActiveRecord::Migration[5.2]
  def change
    create_table :d_jisyaseihins do |t|
      t.string :jyutyuno
      t.string :kokyaku
      t.string :kana
      t.string :zuban
      t.string :meisyo1
      t.string :meisyo2
      t.integer :suryo
      t.date :noki
      t.integer :sijisyochk1
      t.integer :sijisyochk2
      t.integer :sijisyochk3
      t.integer :sijisyochk4
      t.integer :sijisyochk5
      t.integer :sijisyochk6
      t.integer :sijisyochk7
      t.integer :sijisyochk8
      t.integer :sijisyochk9
      t.integer :sijisyochk10
      t.text :biko
      t.text :oborgaki

      t.timestamps
    end
  end
end
