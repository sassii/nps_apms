class CreateUlists < ActiveRecord::Migration[5.2]
  def change
    create_table :ulists do |t|
      t.string :code
      t.string :name
      t.string :director
      t.string :univ
      t.string :faculty
      t.string :department
      t.string :laboratory
      t.string :zipcode
      t.text :address
      t.string :tel
      t.string :email_add
      t.integer :order_amount
      t.integer :order_times
      t.date :order_last
      t.date :estimate_last

      t.timestamps
    end
  end
end
