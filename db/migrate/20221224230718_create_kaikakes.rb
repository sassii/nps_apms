class CreateKaikakes < ActiveRecord::Migration[5.2]
  def change
    create_table :kaikakes do |t|
      t.date :nengetu
      t.string :siharaicd
      t.string :kana
      t.string :hoho
      t.integer :zenkuri
      t.integer :yotei
      t.integer :gokei
      t.integer :hikiotosi
      t.integer :hurikomi
      t.integer :genkin
      t.integer :sousai
      t.integer :tegata
      t.integer :kogite
      t.integer :sonota
      t.integer :kurikosi
      t.integer :sime_kbn
      t.text :biko

      t.timestamps
    end
  end
end
