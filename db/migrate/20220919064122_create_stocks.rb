class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks do |t|
      t.string :item_cd
      t.integer :zaikosu
      t.date :kosinbi
      t.integer :tanka
      t.string :place
      t.text :stock_biko

      t.timestamps
    end
  end
end
