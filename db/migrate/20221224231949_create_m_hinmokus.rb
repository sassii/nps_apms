class CreateMHinmokus < ActiveRecord::Migration[5.2]
  def change
    create_table :m_hinmokus do |t|
      t.string :hinmokucd
      t.string :hinmei
      t.string :zuban
      t.string :kokyakucd
      t.string :gaityucd1
      t.string :gaityunaiyo1
      t.string :gaityucd2
      t.string :gaityunaiyo2
      t.string :gaityucd3
      t.string :gaityunaiyo3
      t.string :gaityucd4
      t.string :gaityunaiyo4
      t.string :gaityucd5
      t.string :gaityunaiyo5
      t.string :gaityucd6
      t.string :gaityunaiyo6
      t.string :gaityucd7
      t.string :gaityunaiyo7
      t.string :gaityucd8
      t.string :gaityunaiyo8
      t.string :gaityucd9
      t.string :gaityunaiyo9
      t.string :gaityucd10
      t.string :gaityunaiyo10
      t.text :biko

      t.timestamps
    end
  end
end
