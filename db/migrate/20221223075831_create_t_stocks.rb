class CreateTStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :t_stocks do |t|
      t.integer :kbn
      t.string :hinmokucd
      t.date :kosinbi
      t.integer :suryo
      t.integer :tanka
      t.string :kosinsya
      t.string :jyutyuno
      t.string :hattyuno
      t.text :biko
      t.integer :yuko_kbn

      t.timestamps
    end
  end
end
