class AddNameToNumbers < ActiveRecord::Migration[5.2]
  def change
    add_column :numbers, :name, :string
  end
end
