# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_09_06_104959) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "d_jisyaseihins", force: :cascade do |t|
    t.string "jyutyuno"
    t.string "kokyaku"
    t.string "kana"
    t.string "zuban"
    t.string "meisyo1"
    t.string "meisyo2"
    t.integer "suryo"
    t.date "noki"
    t.integer "sijisyochk1"
    t.integer "sijisyochk2"
    t.integer "sijisyochk3"
    t.integer "sijisyochk4"
    t.integer "sijisyochk5"
    t.integer "sijisyochk6"
    t.integer "sijisyochk7"
    t.integer "sijisyochk8"
    t.integer "sijisyochk9"
    t.integer "sijisyochk10"
    t.text "biko"
    t.text "oborgaki"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "d_jsyokais", force: :cascade do |t|
    t.string "jyutyuno"
    t.string "kokyaku"
    t.string "zuban"
    t.string "hinmei"
    t.integer "suryo"
    t.integer "tanka"
    t.integer "kin"
    t.date "noki"
    t.date "kanryobi"
    t.text "biko"
    t.integer "gai_kbn"
    t.text "oboegaki"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "d_nohisyos", force: :cascade do |t|
    t.string "uriageno"
    t.integer "uriageeda"
    t.date "uriagebi"
    t.string "kokyakucd"
    t.string "kokyaku"
    t.string "yubin"
    t.string "jyusyo1"
    t.string "jyusyo2"
    t.string "tanto"
    t.string "zuban"
    t.string "hinmei"
    t.integer "suryo"
    t.string "tani"
    t.integer "tanka"
    t.integer "kin"
    t.text "biko"
    t.string "jyutyuno"
    t.integer "jyutyueda"
    t.text "tekiyo"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string "employee_name"
    t.string "skill"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gaityus", force: :cascade do |t|
    t.string "gai_cd"
    t.string "gai_name"
    t.string "gai_kana"
    t.string "gai_ryaku"
    t.string "gai_naiyo"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "kokyaku_id"
    t.date "issued_date"
    t.datetime "sent_at"
    t.string "pdf_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.string "pdf_url"
  end

  create_table "items", force: :cascade do |t|
    t.string "item_cd"
    t.string "name"
    t.string "zuban"
    t.string "kokyakucd"
    t.string "aaaaa"
    t.string "gaityucd1"
    t.string "gaityunaiyo1"
    t.string "gaityucd2"
    t.string "gaityunaiyo2"
    t.string "gaityucd3"
    t.string "gaityunaiyo3"
    t.string "gaityucd4"
    t.string "gaityunaiyo4"
    t.string "gaityucd5"
    t.string "gaityunaiyo5"
    t.string "gaityucd6"
    t.string "gaityunaiyo6"
    t.string "gaityucd7"
    t.string "gaityunaiyo7"
    t.string "gaityucd8"
    t.string "gaityunaiyo8"
    t.string "gaityucd9"
    t.string "gaityunaiyo9"
    t.string "gaityucd10"
    t.string "gaityunaiyo10"
    t.text "item_biko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "j_orders", force: :cascade do |t|
    t.string "jyutyuno"
    t.date "jyutyubi"
    t.string "seikyucd"
    t.string "kojinmei"
    t.string "kaisyamei"
    t.string "zipcd"
    t.text "jyusyo"
    t.string "telno"
    t.string "furigana"
    t.string "jyusyomeisyo"
    t.string "nohin_kojinmei"
    t.string "nohin_kaisyamei"
    t.string "nohin_zipcd"
    t.text "nohin_jyusyo"
    t.string "nohin_telno"
    t.string "nohin_furigana"
    t.integer "tax"
    t.integer "soryo"
    t.integer "daibiki"
    t.integer "sogaku"
    t.integer "seikyusyokbn"
    t.integer "mitumorisyokbn"
    t.integer "ryosyusyokbn"
    t.date "ryosyusyohakkobi"
    t.string "syoruibiko"
    t.string "zenkaisyoruibiko"
    t.integer "photokbn"
    t.integer "konpokbn"
    t.string "konposya"
    t.string "zenkaikonposya"
    t.integer "henkyakukbn"
    t.string "hassohoho"
    t.text "hassobiko"
    t.string "zenkaihassobiko"
    t.text "sijisyobiko"
    t.text "sijisyobiko2"
    t.text "tekiyo"
    t.integer "furikomikbn"
    t.date "hurikomibi"
    t.date "noki"
    t.string "sijisyochk1"
    t.string "sijisyochk2"
    t.string "sijisyochk3"
    t.string "sijisyochk4"
    t.string "sijisyochk5"
    t.string "sijisyochk6"
    t.string "sijisyochk7"
    t.string "sijisyochk8"
    t.string "sijisyochk9"
    t.string "sijisyochk10"
    t.string "daigakucd"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "youto"
    t.string "pgmname"
  end

  create_table "kaikakes", force: :cascade do |t|
    t.date "nengetu"
    t.string "siharaicd"
    t.string "kana"
    t.string "hoho"
    t.integer "zenkuri"
    t.integer "yotei"
    t.integer "gokei"
    t.integer "hikiotosi"
    t.integer "hurikomi"
    t.integer "genkin"
    t.integer "sousai"
    t.integer "tegata"
    t.integer "kogite"
    t.integer "sonota"
    t.integer "kurikosi"
    t.integer "sime_kbn"
    t.text "biko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "shiharaimei"
  end

  create_table "kakakus", force: :cascade do |t|
    t.string "scode"
    t.string "hinban"
    t.integer "kakaku"
    t.text "biko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "kokyakus", force: :cascade do |t|
    t.string "kokyakucd"
    t.string "kokyaku"
    t.string "kana"
    t.string "meisyo"
    t.string "s_atena"
    t.string "s_atena2"
    t.string "gyosyu"
    t.string "tiiki"
    t.integer "simebi"
    t.string "jyoken"
    t.integer "kaisyu"
    t.string "site"
    t.string "kai_jyoken1"
    t.string "kai_naiyo1"
    t.string "tegata"
    t.string "kai_jyoken2"
    t.string "kai_naiyo2"
    t.string "tegata2"
    t.integer "tesuryo"
    t.string "eigyo_tanto"
    t.string "keiri_tanto"
    t.string "zipcd"
    t.string "jyusyo1"
    t.string "jyusyo2"
    t.string "telno"
    t.string "s_zipcd"
    t.string "s_jyusyo1"
    t.string "s_jyusyo2"
    t.text "s_biko1"
    t.text "s_biko2"
    t.text "s_biko3"
    t.string "tantocd"
    t.integer "sendenkbn"
    t.integer "yuko_kbn"
    t.text "biko"
    t.text "seizobiko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "m_dkokyakus", force: :cascade do |t|
    t.string "dkokyakucd"
    t.string "name"
    t.string "daigaku"
    t.string "gakubu"
    t.string "gaka"
    t.string "kenkyusyo"
    t.string "yubin"
    t.text "jyusyo"
    t.string "keisyo"
    t.string "yakusyoku"
    t.string "telno"
    t.string "mail"
    t.integer "kaisu"
    t.integer "kin"
    t.date "jyutyubi"
    t.date "mitumoribi"
    t.string "status"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "m_hinbans", force: :cascade do |t|
    t.string "hinban"
    t.text "kbn"
    t.integer "suti"
    t.text "biko"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "m_hinmokus", force: :cascade do |t|
    t.string "hinmokucd"
    t.string "hinmei"
    t.string "zuban"
    t.string "kokyakucd"
    t.string "gaityucd1"
    t.string "gaityunaiyo1"
    t.string "gaityucd2"
    t.string "gaityunaiyo2"
    t.string "gaityucd3"
    t.string "gaityunaiyo3"
    t.string "gaityucd4"
    t.string "gaityunaiyo4"
    t.string "gaityucd5"
    t.string "gaityunaiyo5"
    t.string "gaityucd6"
    t.string "gaityunaiyo6"
    t.string "gaityucd7"
    t.string "gaityunaiyo7"
    t.string "gaityucd8"
    t.string "gaityunaiyo8"
    t.string "gaityucd9"
    t.string "gaityunaiyo9"
    t.string "gaityucd10"
    t.string "gaityunaiyo10"
    t.text "biko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "m_meisyos", force: :cascade do |t|
    t.string "sikibetu"
    t.string "meisyo"
    t.integer "sort"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "m_seihins", force: :cascade do |t|
    t.string "hinban"
    t.string "seihinmei"
    t.string "seihinmei2"
    t.integer "tanka"
    t.text "biko"
    t.string "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "m_setteis", force: :cascade do |t|
    t.string "kbn"
    t.string "naiyo1"
    t.integer "naiyo2"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "m_siharais", force: :cascade do |t|
    t.string "siharaicd"
    t.string "siharaimei"
    t.string "kana"
    t.integer "hikikbn"
    t.text "biko"
    t.string "gaityucd"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "siharaiho"
  end

  create_table "m_syosyas", force: :cascade do |t|
    t.string "kokyakucd"
    t.string "syosyamei"
    t.string "sijiyochk1"
    t.string "sijiyochk2"
    t.string "sijiyochk3"
    t.string "sijiyochk4"
    t.string "sijiyochk5"
    t.string "sijiyochk6"
    t.string "sijiyochk7"
    t.string "sijiyochk8"
    t.string "sijiyochk9"
    t.string "sijiyochk10"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "machines", force: :cascade do |t|
    t.string "machine_name"
    t.string "naiyo"
    t.string "place"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.integer "mkbn"
    t.integer "mcode"
    t.string "naiyo"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "numbers", force: :cascade do |t|
    t.string "type"
    t.integer "current"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "name"
  end

  create_table "order_headers", force: :cascade do |t|
    t.string "order_no"
    t.date "order_day"
    t.integer "order_kbn"
    t.integer "order_dkbn"
    t.bigint "tanto_id"
    t.string "order_tantoname"
    t.bigint "tokui_id"
    t.string "order_tokuiname"
    t.string "order_kojin"
    t.string "order_kaisya"
    t.string "order_zip"
    t.text "order_adress"
    t.string "order_tel"
    t.string "order_kana"
    t.integer "order_kin"
    t.integer "order_tax"
    t.integer "order_soryo"
    t.integer "order_daibiki"
    t.integer "order_sogaku"
    t.integer "order_furikomi"
    t.date "order_furikomibi"
    t.text "order_tekiyo"
    t.boolean "doc_seikyu"
    t.boolean "doc_mitumori"
    t.boolean "doc_ryosyu"
    t.date "doc_ryosyubi"
    t.text "doc_biko"
    t.boolean "pac_photo"
    t.boolean "pac_henkyaku"
    t.boolean "pac_konpo"
    t.boolean "pac_konpotanto"
    t.text "pac_biko"
    t.string "snd_way"
    t.text "snd_biko"
    t.text "ssj_biko1"
    t.text "ssjbiko2"
    t.text "kis_biko"
    t.string "nhn_kojin"
    t.string "nhn_kaisya"
    t.string "nhn_zip"
    t.string "nhn_tel"
    t.string "nhn_kana"
    t.string "order_addname"
    t.string "sky_kojin"
    t.string "sky_kaisya"
    t.string "sky_zip"
    t.string "sky_kana"
    t.string "sky_tel"
    t.integer "order_yuko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "nhn_adress"
    t.text "sky_adress"
    t.index ["tanto_id"], name: "index_order_headers_on_tanto_id"
    t.index ["tokui_id"], name: "index_order_headers_on_tokui_id"
  end

  create_table "order_histories", force: :cascade do |t|
    t.integer "ulist_id"
    t.date "o_date"
    t.string "order_no"
    t.string "hinmei"
    t.integer "total"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "order_trns", force: :cascade do |t|
    t.bigint "order_header_id"
    t.string "order_no"
    t.integer "order_eda"
    t.string "trn_hinmoku"
    t.string "trn_zuban"
    t.string "trn_hinmei"
    t.integer "trn_suryo"
    t.integer "trn_sakuseisu"
    t.integer "trn_tanka"
    t.integer "trn_kin"
    t.integer "trn_zei"
    t.integer "trn_zaihi"
    t.integer "trn_prghi"
    t.integer "trn_sts"
    t.date "trn_noki"
    t.date "trn_dnoki"
    t.date "trn_kanryobi"
    t.date "trn_syukabi"
    t.date "trn_furikomibi"
    t.date "trn_uriagebi"
    t.string "trn_uriageno"
    t.integer "trn_uriageeda"
    t.text "trn_biko1"
    t.text "trn_biko2"
    t.text "trn_biko3"
    t.integer "trn_maxeda"
    t.integer "trn_yuko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "image"
    t.index ["order_header_id"], name: "index_order_trns_on_order_header_id"
  end

  create_table "stocks", force: :cascade do |t|
    t.string "item_cd"
    t.integer "zaikosu"
    t.date "kosinbi"
    t.integer "tanka"
    t.string "place"
    t.text "stock_biko"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "support_histories", force: :cascade do |t|
    t.integer "ulist_id"
    t.date "s_date"
    t.string "s_content"
    t.string "s_tanto"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_hatyus", force: :cascade do |t|
    t.string "hatyuno"
    t.integer "hatyueda"
    t.date "hatyubi"
    t.string "tantocd"
    t.string "kokyakucd"
    t.string "gaityucd"
    t.string "zuban"
    t.string "hinmei"
    t.string "naiyo"
    t.integer "suryo"
    t.integer "jtanka"
    t.integer "htanka"
    t.date "noki"
    t.date "gainoki"
    t.date "kanryobi"
    t.string "jyutyuno"
    t.integer "jyutyueda"
    t.string "renban"
    t.integer "status"
    t.integer "tanka_flg"
    t.text "biko"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_mitumoris", force: :cascade do |t|
    t.string "mitumu_no"
    t.integer "mitum_eda"
    t.date "mitumbi"
    t.string "mitumtanto"
    t.string "kokyakucd"
    t.string "kokyaku"
    t.string "kokyaku2"
    t.string "kokyaku_tanto"
    t.text "mitummei"
    t.string "zuban"
    t.string "hinmei"
    t.integer "suryo"
    t.integer "tanka"
    t.integer "kin"
    t.integer "soryo"
    t.text "biko"
    t.text "tekiyo"
    t.integer "s_tanka"
    t.integer "z_tanka"
    t.integer "k_tanaka"
    t.integer "gokei"
    t.integer "status"
    t.date "kanryobi"
    t.text "riyu"
    t.string "gaityucd1"
    t.integer "gaityusuryo1"
    t.integer "gaityutanka1"
    t.integer "uritanka1"
    t.string "gaityucd2"
    t.integer "gaityusuryo2"
    t.integer "gaityutanka2"
    t.integer "uritanka2"
    t.string "gaityucd3"
    t.integer "gaityusuryo3"
    t.integer "gaityutanka3"
    t.integer "uritanka3"
    t.string "gaityucd4"
    t.integer "gaityusuryo4"
    t.integer "gaityutanka4"
    t.integer "uritanka4"
    t.string "gaityucd5"
    t.integer "gaityusuryo5"
    t.integer "gaityutanka5"
    t.integer "uritanka5"
    t.text "syoken"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "gaityunaiyo1"
    t.string "gaityunaiyo2"
    t.string "gaityunaiyo3"
    t.string "gaityunaiyo4"
    t.string "gaityunaiyo5"
  end

  create_table "t_nyukins", force: :cascade do |t|
    t.string "denpyono"
    t.string "kokyakucd"
    t.date "simenengetu"
    t.date "nyukinbi"
    t.text "biko"
    t.string "kaisyu1"
    t.integer "kin1"
    t.text "tekiyo1"
    t.date "kijitu1"
    t.string "kaisyu2"
    t.integer "kin2"
    t.text "tekiyo2"
    t.date "kijitu2"
    t.string "kaisyu3"
    t.integer "kin3"
    t.text "tekiyo3"
    t.date "kijitu3"
    t.string "kaisyu4"
    t.integer "kin4"
    t.text "tekiyo4"
    t.date "kijitu4"
    t.string "kaisyu5"
    t.integer "kin5"
    t.text "tekiyo5"
    t.date "kijitu5"
    t.string "kaisyu6"
    t.integer "kin6"
    t.text "tekiyo6"
    t.date "kijitu7"
    t.string "kaisyu7"
    t.integer "kin7"
    t.text "tekiyo7"
    t.integer "tesuryo"
    t.integer "nebiki"
    t.integer "gokei"
    t.string "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "kijitu6"
  end

  create_table "t_orders", force: :cascade do |t|
    t.string "jyutyuno"
    t.integer "jyutyueda"
    t.integer "jyutyukbn"
    t.integer "tyokusokbn"
    t.date "jyutyubi"
    t.string "tanto_id"
    t.string "kokyaku_id"
    t.string "hinmokucd"
    t.string "zuban"
    t.string "hinmei"
    t.integer "suryo"
    t.integer "sakuseisu"
    t.string "tani"
    t.integer "tanka"
    t.integer "kin"
    t.integer "zei"
    t.integer "zairyohi"
    t.integer "programhi"
    t.integer "status"
    t.date "noki"
    t.date "kanryobi"
    t.date "hasonoki"
    t.date "syukabi"
    t.date "hurikomibi"
    t.string "uriageno"
    t.integer "uriageeda"
    t.date "genzainoki"
    t.string "gaityucd1"
    t.string "gaityunaiyo1"
    t.date "gaityunoki1"
    t.date "gaityukanryobi1"
    t.integer "gaityusuryo1"
    t.integer "gaityutanka1"
    t.string "gaityucd2"
    t.string "gaityunaiyo2"
    t.date "gaityunoki2"
    t.date "gaityukanryobi2"
    t.integer "gaityusuryo2"
    t.integer "gaityutanka2"
    t.string "gaityucd3"
    t.string "gaityunaiyo3"
    t.date "gaityunoki3"
    t.date "gaityukanryobi3"
    t.integer "gaityusuryo3"
    t.integer "gaityutanka3"
    t.string "gaityucd4"
    t.string "gaityunaiyo4"
    t.date "gaityunoki4"
    t.date "gaityukanryobi4"
    t.integer "gaityusuryo4"
    t.integer "gaityutanka4"
    t.string "gaityucd5"
    t.string "gaityunaiyo5"
    t.date "gaityunoki5"
    t.date "gaityukanryobi5"
    t.integer "gaityusuryo5"
    t.integer "gaityutanka5"
    t.string "gaityucd6"
    t.string "gaityunaiyo6"
    t.date "gaityunoki6"
    t.date "gaityukanryobi6"
    t.integer "gaityusuryo6"
    t.integer "gaityutanka6"
    t.string "gaityucd7"
    t.string "gaityunaiyo7"
    t.date "gaityunoki7"
    t.date "gaityukanryobi7"
    t.integer "gaityusuryo7"
    t.integer "gaityutanka7"
    t.string "gaityucd8"
    t.string "gaityunaiyo8"
    t.date "gaityunoki8"
    t.date "gaityukanryobi8"
    t.integer "gaityusuryo8"
    t.integer "gaityutanka8"
    t.string "gaityucd9"
    t.string "gaityunaiyo9"
    t.date "gaityunoki9"
    t.date "gaityukanryobi9"
    t.integer "gaityusuryo9"
    t.integer "gaityutanka9"
    t.string "gaityucd10"
    t.string "gaityunaiyo10"
    t.date "gaityunoki10"
    t.date "gaityukanryobi10"
    t.integer "gaityusuryo10"
    t.integer "gaityutanka10"
    t.string "ryakusyo"
    t.integer "seizokbn"
    t.integer "hyomenkbn"
    t.date "hatyunoki"
    t.text "biko"
    t.text "syoken"
    t.string "seikyu_atena"
    t.string "seikyu_atena2"
    t.string "seikyu_zipcd"
    t.string "seikyu_jyusyo"
    t.string "seikyu_jyusyo2"
    t.integer "furikomikbn"
    t.integer "kakekbn"
    t.integer "siharaisite"
    t.string "nohin_zipcd"
    t.string "nohin_furigana"
    t.string "nohin_atena"
    t.string "nohin_atena2"
    t.string "nohin_jyusyo"
    t.string "nohin_jyusyo2"
    t.integer "seikyusyokbn"
    t.integer "mitumorisyokbn"
    t.integer "ryosyusyokbn"
    t.integer "yuko_kbn"
    t.integer "sijisyohakkokbn"
    t.integer "maxeda"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.date "uriagebi"
    t.date "kanseibi"
    t.index ["jyutyuno", "zuban", "hinmei"], name: "index_t_orders_on_jyutyuno_and_zuban_and_hinmei"
    t.index ["noki", "kanryobi", "uriagebi"], name: "index_t_orders_on_noki_and_kanryobi_and_uriagebi"
  end

  create_table "t_rirekis", force: :cascade do |t|
    t.string "supportno"
    t.integer "supporteda"
    t.string "dkokyakucd"
    t.date "kaisibi"
    t.date "kosinbi"
    t.date "supportbi"
    t.string "tanto"
    t.text "naiyo"
    t.string "keka"
    t.text "biko"
    t.integer "status"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_seikyuhds", force: :cascade do |t|
    t.string "kokyakucd"
    t.date "seikyunengetu"
    t.integer "zenkaiseikyu"
    t.integer "nyukin"
    t.integer "kurikosi"
    t.integer "keijyo"
    t.integer "zei"
    t.integer "seikyu"
    t.integer "seikyugokei"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_seikyumeisais", force: :cascade do |t|
    t.string "kokyakucd"
    t.string "kokyaku"
    t.date "simenengetu"
    t.date "uriagebi"
    t.integer "uri_kbn"
    t.string "uriageno"
    t.integer "uriageeda"
    t.string "zuban"
    t.string "hinmei"
    t.integer "suryo"
    t.string "tani"
    t.integer "tanka"
    t.integer "kin"
    t.integer "zei"
    t.text "biko"
    t.string "jyutyuno"
    t.integer "jyutyueda"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_seikyusimebis", force: :cascade do |t|
    t.string "kokyakucd"
    t.date "simenengetu"
    t.integer "simebi"
    t.integer "keisu"
    t.integer "kin"
    t.integer "zeiritu"
    t.integer "zei"
    t.integer "s_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_seikyusyos", force: :cascade do |t|
    t.string "seikyuno"
    t.integer "seikyueda"
    t.string "atena1"
    t.string "atena2"
    t.string "tanto"
    t.string "yubin"
    t.text "biko1"
    t.text "biko2"
    t.text "biko3"
    t.text "jyusyo1"
    t.text "jyusyo2"
    t.integer "zenkaiseikyu"
    t.integer "nyukin"
    t.integer "kurikosi"
    t.integer "keijyo"
    t.integer "zeigaku"
    t.integer "seikyu"
    t.integer "seikyugokei"
    t.integer "soryo"
    t.date "uriagebi"
    t.string "uriageno"
    t.integer "uriageeda"
    t.string "jyutyuno"
    t.integer "jyutyueda"
    t.string "zuban"
    t.string "hinmei"
    t.integer "suryo"
    t.string "tani"
    t.integer "tanka"
    t.integer "kin"
    t.integer "zei"
    t.text "biko"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_stocks", force: :cascade do |t|
    t.integer "kbn"
    t.string "hinmokucd"
    t.date "kosinbi"
    t.integer "suryo"
    t.integer "tanka"
    t.string "kosinsya"
    t.string "jyutyuno"
    t.string "hattyuno"
    t.text "biko"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_uriages", force: :cascade do |t|
    t.string "uriageno"
    t.integer "uriageeda"
    t.date "uriagebi"
    t.integer "uri_kbn"
    t.string "jyutyuno"
    t.integer "jyutyueda"
    t.string "kokyakucd"
    t.string "zuban"
    t.string "hinmei"
    t.integer "suryo"
    t.string "tani"
    t.integer "tanka"
    t.integer "kin"
    t.integer "zei"
    t.text "biko"
    t.integer "sime_kbn"
    t.integer "yuko_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "t_yoteis", force: :cascade do |t|
    t.date "nengetu"
    t.date "yoteibi"
    t.string "kokyakucd"
    t.integer "yoteikin"
    t.date "nyukinbi"
    t.integer "kin"
    t.integer "tesuryo"
    t.integer "nebiki"
    t.integer "zankin"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "tantos", force: :cascade do |t|
    t.string "tanto_cd"
    t.string "tanto_name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "tokuis", force: :cascade do |t|
    t.string "tokui_cd"
    t.string "tokui_name"
    t.string "tokui_kana"
    t.string "tokui_meisyo"
    t.string "seikyu_atena1"
    t.string "seikyu_atena2"
    t.string "tokui_gyosyu"
    t.string "tokui_tiiki"
    t.integer "tokui_simebi"
    t.string "tokui_jyoken"
    t.integer "kaisyu_bi"
    t.string "kaisyu_site"
    t.string "kaisyu_jyiken1"
    t.string "kaisyu_naiyo1"
    t.string "kaisyu_tegata1"
    t.string "kaisyu_jyiken2"
    t.string "kaisyu_naiyo2"
    t.string "kaisyu_tegata2"
    t.integer "kaisyu_tesuryo"
    t.string "tokui_tanto"
    t.string "tokui_keiri"
    t.string "tokui_zip"
    t.string "tokui_adress1"
    t.string "tokui_adress2"
    t.string "tokui_tel"
    t.string "seikyu_zip"
    t.string "seikyu_adress1"
    t.string "seikyu_adress2"
    t.text "seikyu_biko1"
    t.text "seikyu_biko2"
    t.text "seikyu_biko3"
    t.string "seikyu_tanto"
    t.integer "senden_kbn"
    t.integer "yuko_kbn"
    t.integer "seizo_kbn"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "ulists", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "director"
    t.string "univ"
    t.string "faculty"
    t.string "department"
    t.string "laboratory"
    t.string "zipcode"
    t.text "address"
    t.string "tel"
    t.string "email_add"
    t.integer "order_amount"
    t.integer "order_times"
    t.date "order_last"
    t.date "estimate_last"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "role"
    t.string "name"
    t.text "biko"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "work_items", force: :cascade do |t|
    t.integer "wprocess_id"
    t.string "work_item_name"
    t.integer "quantity"
    t.integer "employee_id"
    t.integer "machine_id"
    t.decimal "planned_hours"
    t.decimal "actual_hours"
    t.date "actual_start_date"
    t.date "actual_end_date"
    t.time "actual_start_time"
    t.time "actual_end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "work_progresses", force: :cascade do |t|
    t.integer "work_item_id"
    t.integer "status_id"
    t.date "progress_date"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "work_statuses", force: :cascade do |t|
    t.string "status_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wprocesses", force: :cascade do |t|
    t.integer "t_order_id"
    t.string "process_no"
    t.string "gaityucd"
    t.string "process_name"
    t.date "start_date"
    t.date "end_date"
    t.date "actual_start_date"
    t.date "actual_end_date"
    t.date "deadline"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "order_headers", "tantos"
  add_foreign_key "order_headers", "tokuis"
  add_foreign_key "order_trns", "order_headers"
end
