# coding: utf-8

require "csv"

CSV.foreach('db/ulists.csv') do |row|
    Ulist.create(:code => row[0],
                 :name => row[7],
                 :director => row[9],
                 :univ => row[1],
                 :faculty => row[2],
                 :department=> row[3],
                 :laboratory => row[6],
                 :zipcode    => row[4],
                 :address => row[5],
                 :tel => row[10],
                 :email_add => row[11],
                 :order_amount => row[12],
                 :order_times => row[13],
                 :order_last => row[14],
                 :estimate_last => row[15]
                 )
  end
  