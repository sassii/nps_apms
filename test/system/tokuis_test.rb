require "application_system_test_case"

class TokuisTest < ApplicationSystemTestCase
  setup do
    @tokui = tokuis(:one)
  end

  test "visiting the index" do
    visit tokuis_url
    assert_selector "h1", text: "Tokuis"
  end

  test "creating a Tokui" do
    visit tokuis_url
    click_on "New Tokui"

    fill_in "Kaisyu bi", with: @tokui.kaisyu_bi
    fill_in "Kaisyu jyiken1", with: @tokui.kaisyu_jyiken1
    fill_in "Kaisyu jyiken2", with: @tokui.kaisyu_jyiken2
    fill_in "Kaisyu naiyo1", with: @tokui.kaisyu_naiyo1
    fill_in "Kaisyu naiyo2", with: @tokui.kaisyu_naiyo2
    fill_in "Kaisyu site", with: @tokui.kaisyu_site
    fill_in "Kaisyu tegata1", with: @tokui.kaisyu_tegata1
    fill_in "Kaisyu tegata2", with: @tokui.kaisyu_tegata2
    fill_in "Kaisyu tesuryo", with: @tokui.kaisyu_tesuryo
    fill_in "Seikyu adress1", with: @tokui.seikyu_adress1
    fill_in "Seikyu adress2", with: @tokui.seikyu_adress2
    fill_in "Seikyu atena1", with: @tokui.seikyu_atena1
    fill_in "Seikyu atena2", with: @tokui.seikyu_atena2
    fill_in "Seikyu biko1", with: @tokui.seikyu_biko1
    fill_in "Seikyu biko2", with: @tokui.seikyu_biko2
    fill_in "Seikyu biko3", with: @tokui.seikyu_biko3
    fill_in "Seikyu tanto", with: @tokui.seikyu_tanto
    fill_in "Seikyu zip", with: @tokui.seikyu_zip
    fill_in "Seizo kbn", with: @tokui.seizo_kbn
    fill_in "Senden kbn", with: @tokui.senden_kbn
    fill_in "Tokui adress1", with: @tokui.tokui_adress1
    fill_in "Tokui adress2", with: @tokui.tokui_adress2
    fill_in "Tokui cd", with: @tokui.tokui_cd
    fill_in "Tokui gyosyu", with: @tokui.tokui_gyosyu
    fill_in "Tokui jyoken", with: @tokui.tokui_jyoken
    fill_in "Tokui kana", with: @tokui.tokui_kana
    fill_in "Tokui keiri", with: @tokui.tokui_keiri
    fill_in "Tokui meisyo", with: @tokui.tokui_meisyo
    fill_in "Tokui name", with: @tokui.tokui_name
    fill_in "Tokui simebi", with: @tokui.tokui_simebi
    fill_in "Tokui tanto", with: @tokui.tokui_tanto
    fill_in "Tokui tel", with: @tokui.tokui_tel
    fill_in "Tokui tiiki", with: @tokui.tokui_tiiki
    fill_in "Tokui zip", with: @tokui.tokui_zip
    fill_in "Yuko kbn", with: @tokui.yuko_kbn
    click_on "Create Tokui"

    assert_text "Tokui was successfully created"
    click_on "Back"
  end

  test "updating a Tokui" do
    visit tokuis_url
    click_on "Edit", match: :first

    fill_in "Kaisyu bi", with: @tokui.kaisyu_bi
    fill_in "Kaisyu jyiken1", with: @tokui.kaisyu_jyiken1
    fill_in "Kaisyu jyiken2", with: @tokui.kaisyu_jyiken2
    fill_in "Kaisyu naiyo1", with: @tokui.kaisyu_naiyo1
    fill_in "Kaisyu naiyo2", with: @tokui.kaisyu_naiyo2
    fill_in "Kaisyu site", with: @tokui.kaisyu_site
    fill_in "Kaisyu tegata1", with: @tokui.kaisyu_tegata1
    fill_in "Kaisyu tegata2", with: @tokui.kaisyu_tegata2
    fill_in "Kaisyu tesuryo", with: @tokui.kaisyu_tesuryo
    fill_in "Seikyu adress1", with: @tokui.seikyu_adress1
    fill_in "Seikyu adress2", with: @tokui.seikyu_adress2
    fill_in "Seikyu atena1", with: @tokui.seikyu_atena1
    fill_in "Seikyu atena2", with: @tokui.seikyu_atena2
    fill_in "Seikyu biko1", with: @tokui.seikyu_biko1
    fill_in "Seikyu biko2", with: @tokui.seikyu_biko2
    fill_in "Seikyu biko3", with: @tokui.seikyu_biko3
    fill_in "Seikyu tanto", with: @tokui.seikyu_tanto
    fill_in "Seikyu zip", with: @tokui.seikyu_zip
    fill_in "Seizo kbn", with: @tokui.seizo_kbn
    fill_in "Senden kbn", with: @tokui.senden_kbn
    fill_in "Tokui adress1", with: @tokui.tokui_adress1
    fill_in "Tokui adress2", with: @tokui.tokui_adress2
    fill_in "Tokui cd", with: @tokui.tokui_cd
    fill_in "Tokui gyosyu", with: @tokui.tokui_gyosyu
    fill_in "Tokui jyoken", with: @tokui.tokui_jyoken
    fill_in "Tokui kana", with: @tokui.tokui_kana
    fill_in "Tokui keiri", with: @tokui.tokui_keiri
    fill_in "Tokui meisyo", with: @tokui.tokui_meisyo
    fill_in "Tokui name", with: @tokui.tokui_name
    fill_in "Tokui simebi", with: @tokui.tokui_simebi
    fill_in "Tokui tanto", with: @tokui.tokui_tanto
    fill_in "Tokui tel", with: @tokui.tokui_tel
    fill_in "Tokui tiiki", with: @tokui.tokui_tiiki
    fill_in "Tokui zip", with: @tokui.tokui_zip
    fill_in "Yuko kbn", with: @tokui.yuko_kbn
    click_on "Update Tokui"

    assert_text "Tokui was successfully updated"
    click_on "Back"
  end

  test "destroying a Tokui" do
    visit tokuis_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tokui was successfully destroyed"
  end
end
