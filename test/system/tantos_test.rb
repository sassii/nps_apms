require "application_system_test_case"

class TantosTest < ApplicationSystemTestCase
  setup do
    @tanto = tantos(:one)
  end

  test "visiting the index" do
    visit tantos_url
    assert_selector "h1", text: "Tantos"
  end

  test "creating a Tanto" do
    visit tantos_url
    click_on "New Tanto"

    fill_in "Tanto cd", with: @tanto.tanto_cd
    fill_in "Tanto name", with: @tanto.tanto_name
    click_on "Create Tanto"

    assert_text "Tanto was successfully created"
    click_on "Back"
  end

  test "updating a Tanto" do
    visit tantos_url
    click_on "Edit", match: :first

    fill_in "Tanto cd", with: @tanto.tanto_cd
    fill_in "Tanto name", with: @tanto.tanto_name
    click_on "Update Tanto"

    assert_text "Tanto was successfully updated"
    click_on "Back"
  end

  test "destroying a Tanto" do
    visit tantos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tanto was successfully destroyed"
  end
end
