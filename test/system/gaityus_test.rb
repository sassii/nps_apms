require "application_system_test_case"

class GaityusTest < ApplicationSystemTestCase
  setup do
    @gaityu = gaityus(:one)
  end

  test "visiting the index" do
    visit gaityus_url
    assert_selector "h1", text: "Gaityus"
  end

  test "creating a Gaityu" do
    visit gaityus_url
    click_on "New Gaityu"

    fill_in "Gai cd", with: @gaityu.gai_cd
    fill_in "Gai kana", with: @gaityu.gai_kana
    fill_in "Gai naiyo", with: @gaityu.gai_naiyo
    fill_in "Gai name", with: @gaityu.gai_name
    fill_in "Gai ryaku", with: @gaityu.gai_ryaku
    fill_in "Yuko kbn", with: @gaityu.yuko_kbn
    click_on "Create Gaityu"

    assert_text "Gaityu was successfully created"
    click_on "Back"
  end

  test "updating a Gaityu" do
    visit gaityus_url
    click_on "Edit", match: :first

    fill_in "Gai cd", with: @gaityu.gai_cd
    fill_in "Gai kana", with: @gaityu.gai_kana
    fill_in "Gai naiyo", with: @gaityu.gai_naiyo
    fill_in "Gai name", with: @gaityu.gai_name
    fill_in "Gai ryaku", with: @gaityu.gai_ryaku
    fill_in "Yuko kbn", with: @gaityu.yuko_kbn
    click_on "Update Gaityu"

    assert_text "Gaityu was successfully updated"
    click_on "Back"
  end

  test "destroying a Gaityu" do
    visit gaityus_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Gaityu was successfully destroyed"
  end
end
