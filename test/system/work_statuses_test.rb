require "application_system_test_case"

class WorkStatusesTest < ApplicationSystemTestCase
  setup do
    @work_status = work_statuses(:one)
  end

  test "visiting the index" do
    visit work_statuses_url
    assert_selector "h1", text: "Work statuses"
  end

  test "should create work status" do
    visit work_statuses_url
    click_on "New work status"

    fill_in "Status name", with: @work_status.status_name
    click_on "Create Work status"

    assert_text "Work status was successfully created"
    click_on "Back"
  end

  test "should update Work status" do
    visit work_status_url(@work_status)
    click_on "Edit this work status", match: :first

    fill_in "Status name", with: @work_status.status_name
    click_on "Update Work status"

    assert_text "Work status was successfully updated"
    click_on "Back"
  end

  test "should destroy Work status" do
    visit work_status_url(@work_status)
    click_on "Destroy this work status", match: :first

    assert_text "Work status was successfully destroyed"
  end
end
