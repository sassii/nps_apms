require 'test_helper'

class UlistsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get ulists_index_url
    assert_response :success
  end

  test "should get show" do
    get ulists_show_url
    assert_response :success
  end

  test "should get edit" do
    get ulists_edit_url
    assert_response :success
  end

end
