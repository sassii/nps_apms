require 'test_helper'

class OrderHeadersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get order_headers_index_url
    assert_response :success
  end

  test "should get show" do
    get order_headers_show_url
    assert_response :success
  end

end
