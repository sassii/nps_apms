require 'test_helper'

class GaityusControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gaityu = gaityus(:one)
  end

  test "should get index" do
    get gaityus_url
    assert_response :success
  end

  test "should get new" do
    get new_gaityu_url
    assert_response :success
  end

  test "should create gaityu" do
    assert_difference('Gaityu.count') do
      post gaityus_url, params: { gaityu: { gai_cd: @gaityu.gai_cd, gai_kana: @gaityu.gai_kana, gai_naiyo: @gaityu.gai_naiyo, gai_name: @gaityu.gai_name, gai_ryaku: @gaityu.gai_ryaku, yuko_kbn: @gaityu.yuko_kbn } }
    end

    assert_redirected_to gaityu_url(Gaityu.last)
  end

  test "should show gaityu" do
    get gaityu_url(@gaityu)
    assert_response :success
  end

  test "should get edit" do
    get edit_gaityu_url(@gaityu)
    assert_response :success
  end

  test "should update gaityu" do
    patch gaityu_url(@gaityu), params: { gaityu: { gai_cd: @gaityu.gai_cd, gai_kana: @gaityu.gai_kana, gai_naiyo: @gaityu.gai_naiyo, gai_name: @gaityu.gai_name, gai_ryaku: @gaityu.gai_ryaku, yuko_kbn: @gaityu.yuko_kbn } }
    assert_redirected_to gaityu_url(@gaityu)
  end

  test "should destroy gaityu" do
    assert_difference('Gaityu.count', -1) do
      delete gaityu_url(@gaityu)
    end

    assert_redirected_to gaityus_url
  end
end
