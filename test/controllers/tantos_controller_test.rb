require 'test_helper'

class TantosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tanto = tantos(:one)
  end

  test "should get index" do
    get tantos_url
    assert_response :success
  end

  test "should get new" do
    get new_tanto_url
    assert_response :success
  end

  test "should create tanto" do
    assert_difference('Tanto.count') do
      post tantos_url, params: { tanto: { tanto_cd: @tanto.tanto_cd, tanto_name: @tanto.tanto_name } }
    end

    assert_redirected_to tanto_url(Tanto.last)
  end

  test "should show tanto" do
    get tanto_url(@tanto)
    assert_response :success
  end

  test "should get edit" do
    get edit_tanto_url(@tanto)
    assert_response :success
  end

  test "should update tanto" do
    patch tanto_url(@tanto), params: { tanto: { tanto_cd: @tanto.tanto_cd, tanto_name: @tanto.tanto_name } }
    assert_redirected_to tanto_url(@tanto)
  end

  test "should destroy tanto" do
    assert_difference('Tanto.count', -1) do
      delete tanto_url(@tanto)
    end

    assert_redirected_to tantos_url
  end
end
