require 'test_helper'

class TokuisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tokui = tokuis(:one)
  end

  test "should get index" do
    get tokuis_url
    assert_response :success
  end

  test "should get new" do
    get new_tokui_url
    assert_response :success
  end

  test "should create tokui" do
    assert_difference('Tokui.count') do
      post tokuis_url, params: { tokui: { kaisyu_bi: @tokui.kaisyu_bi, kaisyu_jyiken1: @tokui.kaisyu_jyiken1, kaisyu_jyiken2: @tokui.kaisyu_jyiken2, kaisyu_naiyo1: @tokui.kaisyu_naiyo1, kaisyu_naiyo2: @tokui.kaisyu_naiyo2, kaisyu_site: @tokui.kaisyu_site, kaisyu_tegata1: @tokui.kaisyu_tegata1, kaisyu_tegata2: @tokui.kaisyu_tegata2, kaisyu_tesuryo: @tokui.kaisyu_tesuryo, seikyu_adress1: @tokui.seikyu_adress1, seikyu_adress2: @tokui.seikyu_adress2, seikyu_atena1: @tokui.seikyu_atena1, seikyu_atena2: @tokui.seikyu_atena2, seikyu_biko1: @tokui.seikyu_biko1, seikyu_biko2: @tokui.seikyu_biko2, seikyu_biko3: @tokui.seikyu_biko3, seikyu_tanto: @tokui.seikyu_tanto, seikyu_zip: @tokui.seikyu_zip, seizo_kbn: @tokui.seizo_kbn, senden_kbn: @tokui.senden_kbn, tokui_adress1: @tokui.tokui_adress1, tokui_adress2: @tokui.tokui_adress2, tokui_cd: @tokui.tokui_cd, tokui_gyosyu: @tokui.tokui_gyosyu, tokui_jyoken: @tokui.tokui_jyoken, tokui_kana: @tokui.tokui_kana, tokui_keiri: @tokui.tokui_keiri, tokui_meisyo: @tokui.tokui_meisyo, tokui_name: @tokui.tokui_name, tokui_simebi: @tokui.tokui_simebi, tokui_tanto: @tokui.tokui_tanto, tokui_tel: @tokui.tokui_tel, tokui_tiiki: @tokui.tokui_tiiki, tokui_zip: @tokui.tokui_zip, yuko_kbn: @tokui.yuko_kbn } }
    end

    assert_redirected_to tokui_url(Tokui.last)
  end

  test "should show tokui" do
    get tokui_url(@tokui)
    assert_response :success
  end

  test "should get edit" do
    get edit_tokui_url(@tokui)
    assert_response :success
  end

  test "should update tokui" do
    patch tokui_url(@tokui), params: { tokui: { kaisyu_bi: @tokui.kaisyu_bi, kaisyu_jyiken1: @tokui.kaisyu_jyiken1, kaisyu_jyiken2: @tokui.kaisyu_jyiken2, kaisyu_naiyo1: @tokui.kaisyu_naiyo1, kaisyu_naiyo2: @tokui.kaisyu_naiyo2, kaisyu_site: @tokui.kaisyu_site, kaisyu_tegata1: @tokui.kaisyu_tegata1, kaisyu_tegata2: @tokui.kaisyu_tegata2, kaisyu_tesuryo: @tokui.kaisyu_tesuryo, seikyu_adress1: @tokui.seikyu_adress1, seikyu_adress2: @tokui.seikyu_adress2, seikyu_atena1: @tokui.seikyu_atena1, seikyu_atena2: @tokui.seikyu_atena2, seikyu_biko1: @tokui.seikyu_biko1, seikyu_biko2: @tokui.seikyu_biko2, seikyu_biko3: @tokui.seikyu_biko3, seikyu_tanto: @tokui.seikyu_tanto, seikyu_zip: @tokui.seikyu_zip, seizo_kbn: @tokui.seizo_kbn, senden_kbn: @tokui.senden_kbn, tokui_adress1: @tokui.tokui_adress1, tokui_adress2: @tokui.tokui_adress2, tokui_cd: @tokui.tokui_cd, tokui_gyosyu: @tokui.tokui_gyosyu, tokui_jyoken: @tokui.tokui_jyoken, tokui_kana: @tokui.tokui_kana, tokui_keiri: @tokui.tokui_keiri, tokui_meisyo: @tokui.tokui_meisyo, tokui_name: @tokui.tokui_name, tokui_simebi: @tokui.tokui_simebi, tokui_tanto: @tokui.tokui_tanto, tokui_tel: @tokui.tokui_tel, tokui_tiiki: @tokui.tokui_tiiki, tokui_zip: @tokui.tokui_zip, yuko_kbn: @tokui.yuko_kbn } }
    assert_redirected_to tokui_url(@tokui)
  end

  test "should destroy tokui" do
    assert_difference('Tokui.count', -1) do
      delete tokui_url(@tokui)
    end

    assert_redirected_to tokuis_url
  end
end
