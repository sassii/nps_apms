require "test_helper"

class WorkItemsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get work_items_new_url
    assert_response :success
  end

  test "should get index" do
    get work_items_index_url
    assert_response :success
  end
end
