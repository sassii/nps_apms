require 'test_helper'

class SupportHistoriesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get support_histories_index_url
    assert_response :success
  end

  test "should get new" do
    get support_histories_new_url
    assert_response :success
  end

  test "should get create" do
    get support_histories_create_url
    assert_response :success
  end

  test "should get edit" do
    get support_histories_edit_url
    assert_response :success
  end

end
