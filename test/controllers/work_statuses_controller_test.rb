require "test_helper"

class WorkStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @work_status = work_statuses(:one)
  end

  test "should get index" do
    get work_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_work_status_url
    assert_response :success
  end

  test "should create work_status" do
    assert_difference("WorkStatus.count") do
      post work_statuses_url, params: { work_status: { status_name: @work_status.status_name } }
    end

    assert_redirected_to work_status_url(WorkStatus.last)
  end

  test "should show work_status" do
    get work_status_url(@work_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_work_status_url(@work_status)
    assert_response :success
  end

  test "should update work_status" do
    patch work_status_url(@work_status), params: { work_status: { status_name: @work_status.status_name } }
    assert_redirected_to work_status_url(@work_status)
  end

  test "should destroy work_status" do
    assert_difference("WorkStatus.count", -1) do
      delete work_status_url(@work_status)
    end

    assert_redirected_to work_statuses_url
  end
end
