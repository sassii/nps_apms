require 'test_helper'

class OrderTrnsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get order_trns_index_url
    assert_response :success
  end

  test "should get show" do
    get order_trns_show_url
    assert_response :success
  end

end
