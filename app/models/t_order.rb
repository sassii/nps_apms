class TOrder < ApplicationRecord
  # kokyakucdを外部キーとして、kokyaku_idを主キーとするKokyakuへの関連付け
  # ここでの :kokyaku は、関連付けられるモデルの単数形にする
  belongs_to :kokyaku, foreign_key: 'kokyaku_id', primary_key: 'kokyakucd'
  belongs_to :tanto, foreign_key: 'tanto_id', primary_key: 'tanto_cd'

  has_many :wprocesses

  belongs_to :gaityu1, foreign_key: 'gaityucd1', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu2, foreign_key: 'gaityucd2', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu3, foreign_key: 'gaityucd3', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu4, foreign_key: 'gaityucd4', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu5, foreign_key: 'gaityucd5', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu6, foreign_key: 'gaityucd6', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu7, foreign_key: 'gaityucd7', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu8, foreign_key: 'gaityucd8', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu9, foreign_key: 'gaityucd9', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true
  belongs_to :gaityu10, foreign_key: 'gaityucd10', primary_key: 'gai_cd', class_name: 'Gaityu', optional: true



end