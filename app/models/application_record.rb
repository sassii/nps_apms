class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  before_save :set_date_only

  private

  def set_date_only
    if created_at.present?
      self.created_at = created_at.to_date
    end

    if updated_at.present?
      self.updated_at = updated_at.to_date
    end
  end
  
end
