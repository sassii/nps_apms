class Kokyaku < ApplicationRecord
  # kokyaku_idを主キーとして、kokyakucdを外部キーとするTOrderへの関連付け
  has_many :t_orders, foreign_key: 'kokyakucd', primary_key: 'kokyaku_id'
  has_many :invoices
end