class WorkItem < ApplicationRecord
    belongs_to :wprocess
    belongs_to :employee
    belongs_to :machine
    has_many :work_progresses, dependent: :destroy
end
