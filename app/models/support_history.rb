class SupportHistory < ApplicationRecord
    belongs_to :ulist, optional: true
  
    default_scope -> { order(s_date: :desc) }
end
