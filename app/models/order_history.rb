class OrderHistory < ApplicationRecord
    belongs_to :ulists, optional: true
  
    default_scope -> { order(o_date: :desc) }
end
