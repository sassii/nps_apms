class Ulist < ApplicationRecord
    has_many :order_histories
    accepts_nested_attributes_for :order_histories, allow_destroy: true, reject_if: :all_blank

    has_many :support_histories
    accepts_nested_attributes_for :support_histories, allow_destroy: true, reject_if: :all_blank

    default_scope -> { order(id: :asc) }
end
