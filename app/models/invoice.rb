class Invoice < ApplicationRecord
    belongs_to :kokyaku

    #mount_uploader :pdf, PdfUploader
  
    # 状態を管理するための定数
    STATUSES = %w[draft sent paid].freeze
  
    validates :status, presence: true, inclusion: { in: STATUSES }
    validates :issued_date, presence: true
    validates :kokyaku_id, uniqueness: { scope: :issued_date, message: "は同一の対象年月に対して複数のインボイスを持つことはできません" }
  
    after_initialize :set_default_status, if: :new_record?
  
    def set_default_status
      self.status ||= 'draft'
    end
  
    # 送信済みかどうかを判定するメソッド
    def sent?
      sent_at.present?
    end
  
    # 支払済みかどうかを判定するメソッド
    def paid?
      paid_at.present?
    end
  
    # 送信日時を設定するメソッド
    def mark_as_sent
      if update(sent_at: Time.current, status: "sent")
        true
      else
        false
      end
    end
  
    # 支払日時を設定するメソッド
    def mark_as_paid
      if update(paid_at: Time.current, status: "paid")
        true
      else
        false
      end
    end
  end
  