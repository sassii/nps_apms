class Wprocess < ApplicationRecord
    has_many :work_items, dependent: :destroy
    belongs_to :t_order 
    has_many :work_progresses
    belongs_to :gaityu, foreign_key: 'gaityucd', primary_key: 'gai_cd'


    accepts_nested_attributes_for :work_items



        # 完了日が設定されていない受注に対してWprocessを作成するクラスメソッド
        def self.create_wprocesses_for_incomplete_t_orders
            TOrder.where(kanryobi: nil).where.not(gaityunoki1: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 1)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end
                t_order.wprocesses.create!(
                  process_name: t_order.gaityunaiyo1, 
                  t_order_id: t_order.id, 
                  process_no: 1, 
                  deadline: t_order.gaityunoki1, 
                  gaityucd: t_order.gaityucd1,
                  work_items_attributes: work_items
                )

              end
            end
      
            TOrder.where(kanryobi: nil).where.not(gaityunoki2: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 2)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end  
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo2, 
                  t_order_id: t_order.id, 
                  process_no: 2, 
                  deadline: t_order.gaityunoki2, 
                  gaityucd: t_order.gaityucd2,
                  work_items_attributes: work_items
                )
              end
            end
      
            TOrder.where(kanryobi: nil).where.not(gaityunoki3: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 3)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end  
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo3, 
                  t_order_id: t_order.id, 
                  process_no: 3, 
                  deadline: t_order.gaityunoki3, 
                  gaityucd: t_order.gaityucd3,
                  work_items_attributes: work_items
                )
              end
            end

            TOrder.where(kanryobi: nil).where.not(gaityunoki4: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 4)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end  
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo4, 
                  t_order_id: t_order.id, 
                  process_no: 4, 
                  deadline: t_order.gaityunoki4, 
                  gaityucd: t_order.gaityucd4,
                  work_items_attributes: work_items
                )
              end
            end

            TOrder.where(kanryobi: nil).where.not(gaityunoki5: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 5)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end  
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo6, 
                  t_order_id: t_order.id, 
                  process_no: 5, 
                  deadline: t_order.gaityunoki5, 
                  gaityucd: t_order.gaityucd5,
                  work_items_attributes: work_items
                )
              end
            end

            TOrder.where(kanryobi: nil).where.not(gaityunoki6: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 6)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo6, 
                  t_order_id: t_order.id, 
                  process_no: 6, 
                  deadline: t_order.gaityunoki6, 
                  gaityucd: t_order.gaityucd6,
                  work_items_attributes: [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo6},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo6},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo6},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo6},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo6},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo6},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo6}
                  ]
                )
              end
            end
            
            
            TOrder.where(kanryobi: nil).where.not(gaityunoki7: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 7)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end  
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo7, 
                  t_order_id: t_order.id, 
                  process_no: 7, 
                  deadline: t_order.gaityunoki7, 
                  gaityucd: t_order.gaityucd7,
                  work_items_attributes: work_items
                )
              end
            end            


            TOrder.where(kanryobi: nil).where.not(gaityunoki8: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 8)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end  
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo8, 
                  t_order_id: t_order.id, 
                  process_no: 8, 
                  deadline: t_order.gaityunoki8, 
                  gaityucd: t_order.gaityucd8,
                  work_items_attributes: work_items
                )
              end
            end            

            TOrder.where(kanryobi: nil).where.not(gaityunoki9: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 9)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end  
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo9, 
                  t_order_id: t_order.id, 
                  process_no: 9, 
                  deadline: t_order.gaityunoki9, 
                  gaityucd: t_order.gaityucd9,
                  work_items_attributes: work_items
                )
              end
            end
            

            TOrder.where(kanryobi: nil).where.not(gaityunoki10: nil).where(yuko_kbn: 0).find_each do |t_order|
              unless t_order.wprocesses.exists?(t_order_id: t_order.id, process_no: 10)
                work_items = if t_order.gaityucd1 == "001"
                  [
                    {work_item_name: 'マシニング', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'センバン', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'フライス', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1}
                  ]
                else
                  [
                    {work_item_name: 'プログラム', employee_id: 1, machine_id: 3, quantity: t_order.gaityusuryo1},
                    {work_item_name: 'レーザー', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '曲げ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '溶接', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'バリ取り', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: 'タップ', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1},
                    {work_item_name: '検査', employee_id: 1, machine_id: 3,quantity: t_order.gaityusuryo1}
                  ]
                end
                t_order.wprocesses.create(
                  process_name: t_order.gaityunaiyo10, 
                  t_order_id: t_order.id, 
                  process_no: 10, 
                  deadline: t_order.gaityunoki10, 
                  gaityucd: t_order.gaityucd10,
                  work_items_attributes: work_items
                )
              end
            end
      end
end
