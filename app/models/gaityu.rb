class Gaityu < ApplicationRecord
    has_many :wprocesses, foreign_key: 'gaityucd', primary_key: 'gai_cd'

    has_many :t_orders1, foreign_key: 'gaityucd1', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders2, foreign_key: 'gaityucd2', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders1, foreign_key: 'gaityucd3', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders2, foreign_key: 'gaityucd4', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders1, foreign_key: 'gaityucd5', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders2, foreign_key: 'gaityucd6', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders1, foreign_key: 'gaityucd7', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders2, foreign_key: 'gaityucd8', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders2, foreign_key: 'gaityucd9', primary_key: 'gai_cd', class_name: 'TOrder'
    has_many :t_orders1, foreign_key: 'gaityucd10', primary_key: 'gai_cd', class_name: 'TOrder'
    
end
