class OrderTrn < ApplicationRecord
  mount_uploader :image, ImagesUploader
  
  belongs_to :order_header
end
