require 'zip'

class InvoiceMailer < ApplicationMailer
  def send_invoice(email, pdf_path, subject, body)
    absolute_pdf_path = Rails.root.join('public', pdf_path)
    Rails.logger.info "Absolute PDF Path: #{absolute_pdf_path}"

    if File.exist?(absolute_pdf_path)
      pdf_content = File.binread(absolute_pdf_path)
      Rails.logger.info "PDF file content size: #{pdf_content.size} bytes"

      begin
        zip_file_path = Rails.root.join('tmp', "#{File.basename(pdf_path, '.pdf')}.zip")

        # 既存のZIPファイルを削除
        File.delete(zip_file_path) if File.exist?(zip_file_path)

        Zip::File.open(zip_file_path, Zip::File::CREATE) do |zipfile|
          timestamp = Time.now.strftime("%Y%m%d%H%M%S")
          zipfile.add("#{File.basename(pdf_path, '.pdf')}_#{timestamp}.pdf", absolute_pdf_path)
        end

        Rails.logger.info "ZIP file created successfully: #{zip_file_path}"

        # ZIPファイルをインライン添付
        attachments.inline['invoice.zip'] = {
          mime_type: 'application/zip',
          content: File.binread(zip_file_path)
        }

      rescue => e
        Rails.logger.error "Failed to create or attach ZIP file: #{e.message}"
      ensure
        File.delete(zip_file_path) if File.exist?(zip_file_path)
      end
    else
      Rails.logger.error "File does not exist: #{absolute_pdf_path}"
    end

    mail(to: email, subject: subject) do |format|
      format.text { render plain: body }
      format.html { render html: body.html_safe }
    end
  rescue => e
    Rails.logger.error "An error occurred while sending email: #{e.message}"
  end
end
