json.extract! gaityu, :id, :gai_cd, :gai_name, :gai_kana, :gai_ryaku, :gai_naiyo, :yuko_kbn, :created_at, :updated_at
json.url gaityu_url(gaityu, format: :json)
