json.extract! tanto, :id, :tanto_cd, :tanto_name, :created_at, :updated_at
json.url tanto_url(tanto, format: :json)
