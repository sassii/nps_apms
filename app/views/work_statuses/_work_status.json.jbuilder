json.extract! work_status, :id, :status_name, :created_at, :updated_at
json.url work_status_url(work_status, format: :json)
