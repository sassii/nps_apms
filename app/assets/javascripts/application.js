import Turbolinks from "turbolinks"
Turbolinks.start()

import Chart from 'chart.js';

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require popper
//= require bootstrap

//= require rails-ujs
//= require activestorage
//= require turbolinks

// application_jquery.js
function remove_fields(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".fields").hide();
}

function add_fields(link, association, content) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().before(content.replace(regexp, new_id));
}

//= require_tree .

//= require chartkick
//= require Chart.bundle

import "chart.js"

import 'chartkick/chart.js'

document.addEventListener('DOMContentLoaded', function() {
    // トグルボタンのクリックイベントリスナーを設定
    document.querySelectorAll('.toggle-details').forEach(function(button) {
      button.addEventListener('click', function(e) {
        e.preventDefault(); // デフォルトのアンカー動作を阻止
        var details = this.parentElement.parentElement.nextElementSibling; // 対応するwork-item-detailsを取得
        if (details.style.display === 'none') {
          details.style.display = ''; // 詳細を表示
          this.textContent = '▲ 詳細非表示'; // ボタンのテキストを変更
        } else {
          details.style.display = 'none'; // 詳細を非表示
          this.textContent = '▼ 詳細表示'; // ボタンのテキストを変更
        }
      });
    });
  });
  
