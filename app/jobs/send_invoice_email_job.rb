class SendInvoiceEmailJob < ApplicationJob
  queue_as :default

  def perform(invoice_id, email_subject, email_body)
    invoice = Invoice.find(invoice_id)
    InvoiceMailer.send_invoice(invoice.kokyaku.gyosyu, invoice.pdf_path, email_subject, email_body).deliver_now
  end
end
