class TOrderController < ApplicationController
  require 'date'
  before_action :authenticate_user!

  def index
    Rails.logger.info "Current user: #{current_user.inspect}"
    Rails.logger.info "Session: #{session.to_hash.inspect}"

    # 選択された年月を取得
    selected_date = params[:date].present? ? Date.parse("#{params[:date]}-01") : Date.today


    # 現在の月の初日
    # 開始日 = Date.today.beginning_of_month
    開始日 = selected_date.beginning_of_month

    # 現在の月の末日
    # 終了日 = Date.today.end_of_month
    終了日 = selected_date.end_of_month

    #今日 = Date.today
    今日 = selected_date
    期初日 = 今日.month > 5 ? Date.new(今日.year, 6, 1) : Date.new(今日.year - 1, 6, 1)

    期末日 = 今日.month > 5 ? Date.new(今日.year + 1, 5, 31) : Date.new(今日.year, 5, 31)

  	@t_orders_by_kokyakus = TOrder.joins(:kokyaku)
                                .select('kokyakus.kokyakucd as kokyaku_id, kokyakus.kokyaku, 
                                         EXTRACT(YEAR FROM t_orders.uriagebi) as year,
                                         SUM(t_orders.kin) as total_amount')
                                .group('kokyakus.kokyakucd, kokyakus.kokyaku, 
                                       EXTRACT(YEAR FROM t_orders.uriagebi)')
                                .order('kokyakus.kokyakucd, year')
                                .where('t_orders.uriagebi is not null')

    @pie_orders_by_kokyakus = TOrder.joins(:kokyaku)
                                .select('kokyakus.kokyakucd as kokyaku_id, kokyakus.kokyaku, SUM(t_orders.kin) as total_amount')
                                .group('kokyakus.kokyakucd, kokyakus.kokyaku')
                                .order('kokyakus.kokyakucd')
                                .where('t_orders.yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?', 開始日, 終了日)

    
    @pie_orders_by_kokyakus1 = TOrder.where('t_orders.yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?', 開始日, 終了日)
    @pie_orders_by_kokyakus2 = @pie_orders_by_kokyakus1.joins(:kokyaku).group('kokyakus.meisyo').sum(:kin).sort_by { |_, v| v }.reverse.to_h

    @pie_orders_n_net1 = TOrder.where("t_orders.kokyaku_id <> '035' AND t_orders.yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?", 開始日, 終了日)
    @pie_orders_n_net2 = @pie_orders_n_net1 .joins(:kokyaku).group('kokyakus.meisyo').sum(:kin).sort_by { |_, v| v }.reverse.to_h
                 
    @pie_orders_by_user1 = TOrder.where('t_orders.yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?', 開始日, 終了日)
    @pie_orders_by_user2 = @pie_orders_by_kokyakus1.joins(:tanto).group('tantos.tanto_name').sum(:kin).sort_by { |_, v| v }.reverse.to_h

    @pie_orders_by_tkokyaku1 = TOrder.where('t_orders.yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?', 期初日, 期末日)
    @pie_orders_by_tkokyaku2 = @pie_orders_by_tkokyaku1 .joins(:kokyaku).group('kokyakus.meisyo').sum(:kin).sort_by { |_, v| v }.reverse.to_h
                      

#当月売上実績
    # 開始日と終了日を基に売上金額の集計を行う
    #@t_uriage = TOrder.select('SUM(t_orders.kin) as total_amount')
    #                  .where('yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?', 開始日, 終了日)

#当月受注実績
    @t_jyutyu = TOrder.select('SUM(t_orders.kin) as total_amount')
                      .where('yuko_kbn = 0 AND t_orders.jyutyubi BETWEEN ? AND ?', 開始日, 終了日)

#当月売上予定
    @t_uriageyo = TOrder.select('SUM(t_orders.kin) as total_amount')
                      .where('yuko_kbn = 0 AND t_orders.noki BETWEEN ? AND ?', 開始日, 終了日)      
                      
#当月未完了
    @t_mikanryo = TOrder.select('SUM(t_orders.kin) as total_amount')
                      .where('yuko_kbn = 0 AND t_orders.kanryobi is null AND t_orders.noki BETWEEN ? AND ?', 開始日, 終了日) 

#次月以降未完了
    #@t_nextmikanryo = TOrder.select('SUM(t_orders.kin) as total_amount')
    #.where('yuko_kbn = 0 AND t_orders.kanryobi is null AND t_orders.noki > ? ' , 終了日)
     
                      
#当月外注金額
    @t_gaityu1 = TOrder.select('SUM(t_orders.gaityutanka1*t_orders.gaityusuryo1) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd1 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )                      
    @t_gaityu2 = TOrder.select('SUM(t_orders.gaityutanka2*t_orders.gaityusuryo2) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd2 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )    
    @t_gaityu3 = TOrder.select('SUM(t_orders.gaityutanka3*t_orders.gaityusuryo3) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd3 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
    @t_gaityu4 = TOrder.select('SUM(t_orders.gaityutanka4*t_orders.gaityusuryo4) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd4 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
    @t_gaityu5 = TOrder.select('SUM(t_orders.gaityutanka5*t_orders.gaityusuryo5) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd5 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
    @t_gaityu6 = TOrder.select('SUM(t_orders.gaityutanka6*t_orders.gaityusuryo6) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd6 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
    @t_gaityu7 = TOrder.select('SUM(t_orders.gaityutanka7*t_orders.gaityusuryo7) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd7 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
    @t_gaityu8 = TOrder.select('SUM(t_orders.gaityutanka8*t_orders.gaityusuryo8) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd8 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
    @t_gaityu9 = TOrder.select('SUM(t_orders.gaityutanka9*t_orders.gaityusuryo9) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd9 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
    @t_gaityu10 = TOrder.select('SUM(t_orders.gaityutanka10*t_orders.gaityusuryo10) as total_amount')
    .where("yuko_kbn = 0 AND t_orders.gaityucd10 <> '000' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )

    @gaityu = 0
    @t_gaityu1.each do |gaityu1|
      if gaityu1.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu1.total_amount end
      end
    @t_gaityu2.each do |gaityu2|
      if gaityu2.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu2.total_amount end
      end
    @t_gaityu3.each do |gaityu3|
      if gaityu3.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu3.total_amount end
      end
    @t_gaityu4.each do |gaityu4|
      if gaityu4.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu4.total_amount end
      end
    @t_gaityu5.each do |gaityu5|
      if gaityu5.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu5.total_amount end
      end
    @t_gaityu6.each do |gaityu6|
      if gaityu6.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu6.total_amount end
      end
    @t_gaityu7.each do |gaityu7|
      if gaityu7.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu7.total_amount end
      end
    @t_gaityu8.each do |gaityu8|
      if gaityu8.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu8.total_amount end
      end
    @t_gaityu9.each do |gaityu9|
      if gaityu9.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu9.total_amount end
      end
    @t_gaityu10.each do |gaityu10|
      if gaityu10.total_amount.nil?    
      else
        @gaityu =  @gaityu + gaityu10.total_amount end
      end

#機械加工事業部金額
@t_kk1 = TOrder.select('SUM(t_orders.gaityutanka1*t_orders.gaityusuryo1) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd1 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )                      
@t_kk2 = TOrder.select('SUM(t_orders.gaityutanka2*t_orders.gaityusuryo2) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd2 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )    
@t_kk3 = TOrder.select('SUM(t_orders.gaityutanka3*t_orders.gaityusuryo3) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd3 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
@t_kk4 = TOrder.select('SUM(t_orders.gaityutanka4*t_orders.gaityusuryo4) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd4 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
@t_kk5 = TOrder.select('SUM(t_orders.gaityutanka5*t_orders.gaityusuryo5) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd5 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
@t_kk6 = TOrder.select('SUM(t_orders.gaityutanka6*t_orders.gaityusuryo6) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd6 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
@t_kk7 = TOrder.select('SUM(t_orders.gaityutanka7*t_orders.gaityusuryo7) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd7 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
@t_kk8 = TOrder.select('SUM(t_orders.gaityutanka8*t_orders.gaityusuryo8) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd8 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
@t_kk9 = TOrder.select('SUM(t_orders.gaityutanka9*t_orders.gaityusuryo9) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd9 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )
@t_kk10 = TOrder.select('SUM(t_orders.gaityutanka10*t_orders.gaityusuryo10) as total_amount')
.where("yuko_kbn = 0 AND t_orders.gaityucd10 = '001' AND t_orders.noki BETWEEN ? AND ?", 開始日, 終了日 )      

@kk=0
@t_kk1.each do |kk1|
  if kk1.total_amount.nil?    
  else
    @kk =  @kk + kk1.total_amount end
  end
@t_kk2.each do |kk2|
  if kk2.total_amount.nil?    
  else
    @kk =  @kk + kk2.total_amount end
  end
@t_kk3.each do |kk3|
  if kk3.total_amount.nil?    
  else
    @kk =  @kk + kk3.total_amount end
  end
@t_kk4.each do |kk4|
  if kk4.total_amount.nil?    
  else
    @kk =  @kk + kk4.total_amount end
  end
@t_kk5.each do |kk5|
  if kk5.total_amount.nil?    
  else
    @kk =  @kk + kk5.total_amount end
  end  
@t_kk6.each do |kk6|
  if kk6.total_amount.nil?    
  else
    @kk =  @kk + kk6.total_amount end
  end
@t_kk7.each do |kk7|
  if kk7.total_amount.nil?    
  else
    @kk =  @kk + kk7.total_amount end
  end
@t_kk8.each do |kk8|
  if kk8.total_amount.nil?    
  else
    @kk =  @kk + kk8.total_amount end
  end
@t_kk9.each do |kk9|
  if kk9.total_amount.nil?    
  else
    @kk =  @kk + kk9.total_amount end
  end
@t_kk10.each do |kk10|
  if kk10.total_amount.nil?    
  else
    @kk =  @kk + kk10.total_amount end
  end

#当期売上実績
@t_turiage = TOrder.select('SUM(t_orders.kin) as total_amount')
                      .where('yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?', 期初日, 期末日)


# 過去5年間の月別売上実績を取得
@monthly_sales = Array.new(12) { |i| { month: (6 + i - 1) % 12 + 1, data: {} } }

(0..4).each do |year_offset|
  fiscal_year_start = Date.current.year - year_offset

  # 売上データを取得する期間
  sales_data = TOrder
    .select("EXTRACT(MONTH FROM uriagebi) AS month, SUM(kin) AS total_amount")
    .where('yuko_kbn = 0 AND uriagebi BETWEEN ? AND ?', 
           Date.new(fiscal_year_start, 6, 1), Date.new(fiscal_year_start + 1, 5, 31))
    .group("EXTRACT(MONTH FROM uriagebi)")
    .order("month")

  sales_data.each do |record|
    month_index = (record.month.to_i + 12 - 6) % 12

    # 6月から12月はその年度、1月から5月は前年度として扱う
    year_key = if record.month.to_i >= 6
                 fiscal_year_start  # 6月から12月はその年度
               else
                 fiscal_year_start   # 1月から5月は前年度
               end

    # 売上データを適切な年度に格納
    @monthly_sales[month_index][:data][year_key] = record.total_amount.to_i
  end

  # 売上データが存在しない月を0円に設定
  @monthly_sales.each_with_index do |month_data, index|
    year_key = fiscal_year_start + (index < 6 ? -1 : 0) # 1月から5月は前年度
    @monthly_sales[index][:data][year_key] ||= 0
  end
end




# 現在の月以前でもデータを取得する
if Date.current.month < 6
  past_year_start = Date.current.year - 5
  sales_data = TOrder
    .select("EXTRACT(MONTH FROM uriagebi) AS month, SUM(kin) AS total_amount")
    .where('yuko_kbn = 0 AND uriagebi BETWEEN ? AND ?', 
           Date.new(past_year_start - 1, 6, 1), Date.new(past_year_start, 5, 31))
    .group("EXTRACT(MONTH FROM uriagebi)")
    .order("month")

  sales_data.each do |record|
    month_index = (record.month.to_i + 12 - 6) % 12
    year_key = past_year_start + (record.month.to_i < 6 ? 1 : 0) # 6月以前は次年度に加算

    # 売上データを適切な年度に格納
    @monthly_sales[month_index][:data][year_key] = record.total_amount.to_i
  end

  # 売上データが存在しない月を0円に設定
  @monthly_sales.each_with_index do |month_data, index|
    year_key = past_year_start + (index < 6 ? 1 : 0) # 6月以前は次年度に加算
    @monthly_sales[index][:data][year_key] ||= 0
  end
end




#売り上げ実績                                
    @t_orders = TOrder.select(
      "CASE 
        WHEN EXTRACT(MONTH FROM t_orders.uriagebi) >= 6 THEN 
          EXTRACT(YEAR FROM t_orders.uriagebi)
        ELSE 
          EXTRACT(YEAR FROM t_orders.uriagebi) - 1
      END as fiscal_year,
      SUM(t_orders.kin) as total_amount"
    )
    .group(
      "CASE 
        WHEN EXTRACT(MONTH FROM t_orders.uriagebi) >= 6 THEN 
          EXTRACT(YEAR FROM t_orders.uriagebi)
        ELSE 
          EXTRACT(YEAR FROM t_orders.uriagebi) - 1
      END"
    )
    .order('fiscal_year')
    .where('t_orders.uriagebi is not null and t_orders.yuko_kbn = 0')


    

  end

  def index_a
    @jyutyu_m = TOrder.all.where('yuko_kbn = 0 and kanryobi is null').order('noki')
  end

  def index_b
    @jyutyu_m = TOrder
      .joins("
      LEFT JOIN gaityus AS g1 ON t_orders.gaityucd1 = g1.gai_cd
      LEFT JOIN gaityus AS g2 ON t_orders.gaityucd2 = g2.gai_cd
      LEFT JOIN gaityus AS g3 ON t_orders.gaityucd3 = g3.gai_cd
      LEFT JOIN gaityus AS g4 ON t_orders.gaityucd4 = g4.gai_cd
      LEFT JOIN gaityus AS g5 ON t_orders.gaityucd5 = g5.gai_cd
      LEFT JOIN gaityus AS g6 ON t_orders.gaityucd6 = g6.gai_cd
      LEFT JOIN gaityus AS g7 ON t_orders.gaityucd7 = g7.gai_cd
      LEFT JOIN gaityus AS g8 ON t_orders.gaityucd8 = g8.gai_cd
      LEFT JOIN gaityus AS g9 ON t_orders.gaityucd9 = g9.gai_cd
      LEFT JOIN gaityus AS g10 ON t_orders.gaityucd10 = g10.gai_cd
    ")
    .select('t_orders.*, g1.gai_ryaku AS gai_ryaku1, g2.gai_ryaku AS gai_ryaku2, g3.gai_ryaku AS gai_ryaku3, g4.gai_ryaku AS gai_ryaku4, g5.gai_ryaku AS gai_ryaku5, g6.gai_ryaku AS gai_ryaku6, g7.gai_ryaku AS gai_ryaku7, g8.gai_ryaku AS gai_ryaku8, g9.gai_ryaku AS gai_ryaku9, g10.gai_ryaku AS gai_ryaku10')
    .where('t_orders.yuko_kbn = 0 AND t_orders.kanryobi IS NULL AND t_orders.seizokbn = 1')
    .order('t_orders.noki')
  end

  def detail
    @order = TOrder.find(params[:id]) # IDに基づいてレコードを取得
    if @order.nil?
      redirect_to orders_path, alert: '受注が見つかりませんでした。'
    end
  end
end

