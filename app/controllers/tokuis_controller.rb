class TokuisController < ApplicationController
  before_action :set_tokui, only: %i[ show edit update destroy ]

  # GET /tokuis or /tokuis.json
  def index
    @tokuis = Tokui.all
  end

  # GET /tokuis/1 or /tokuis/1.json
  def show
  end

  # GET /tokuis/new
  def new
    @tokui = Tokui.new
  end

  # GET /tokuis/1/edit
  def edit
  end

  # POST /tokuis or /tokuis.json
  def create
    @tokui = Tokui.new(tokui_params)

    respond_to do |format|
      if @tokui.save
        format.html { redirect_to @tokui, notice: "Tokui was successfully created." }
        format.json { render :show, status: :created, location: @tokui }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tokui.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tokuis/1 or /tokuis/1.json
  def update
    respond_to do |format|
      if @tokui.update(tokui_params)
        format.html { redirect_to @tokui, notice: "Tokui was successfully updated." }
        format.json { render :show, status: :ok, location: @tokui }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tokui.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tokuis/1 or /tokuis/1.json
  def destroy
    @tokui.destroy
    respond_to do |format|
      format.html { redirect_to tokuis_url, notice: "Tokui was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tokui
      @tokui = Tokui.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tokui_params
      params.require(:tokui).permit(:tokui_cd, :tokui_name, :tokui_kana, :tokui_meisyo, :seikyu_atena1, :seikyu_atena2, :tokui_gyosyu, :tokui_tiiki, :tokui_simebi, :tokui_jyoken, :kaisyu_bi, :kaisyu_site, :kaisyu_jyiken1, :kaisyu_naiyo1, :kaisyu_tegata1, :kaisyu_jyiken2, :kaisyu_naiyo2, :kaisyu_tegata2, :kaisyu_tesuryo, :tokui_tanto, :tokui_keiri, :tokui_zip, :tokui_adress1, :tokui_adress2, :tokui_tel, :seikyu_zip, :seikyu_adress1, :seikyu_adress2, :seikyu_biko1, :seikyu_biko2, :seikyu_biko3, :seikyu_tanto, :senden_kbn, :yuko_kbn, :seizo_kbn)
    end
end
