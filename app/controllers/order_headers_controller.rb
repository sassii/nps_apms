class OrderHeadersController < ApplicationController
  def index
    @q = OrderHeader.ransack(params[:q])
    @orderheaders = @q.result.page params[:page]
  end

  def search
    @q = OrderHeader.ransack(search_params)
    @orderheaders = @q.result
  end

  def show
    @orderheader = OrderHeader.find(params[:id])
    @support_history = SupportHistory.new
    @support_history = SupportHistory.where('ulist_id = ?'  , params[:id])
  end
end
