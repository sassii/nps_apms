class OrderTrnsController < ApplicationController
  def index
    @q = OrderTrn.ransack(params[:q])
    @ordertrns = @q.result.page params[:page]
  end

  def index_image
    @q = OrderTrn.ransack(params[:q])
    @ordertrns = @q.result.page params[:page]
  end

  def search
    @q = OrderTrn.ransack(search_params)
    @ordertrns = @q.result
  end

  def show
    @ordertrns = OrderTrn.find(params[:id])

  end

  def edit
    @ordertrns = OrderTrn.find(params[:id])
    if request.patch? then
      if @ordertrns.update(ordertrn_params)
        redirect_to '/order_trns/index'
      else
        render :edit
      end
    end
  end

  def update
    @ordertrns = OrderTrn.find(params[:id])
    if request.patch? thena
      if @ordertrns.update(ordertrn_params)
        redirect_to '/order_trns/index'
      else
        render :edit
      end
    end
  end



  def search_params
    params.require(:q).permit!
  end

  def ordertrn_params
      params.require(:order_trn).permit(
      :id,
      :order_header_id,
      :order_no,
      :order_eda,
      :trn_hinmoku,
      :trn_zuban,
      :trn_hinmei,
      :trn_suryo,
      :trn_sakuseisu,
      :trn_tanka,
      :trn_kin,
      :trn_zei,
      :trn_zaihi,
      :trn_prghi,
      :trn_sts,
      :trn_noki,
      :trn_dnoki,
      :trn_kanryobi,
      :trn_syukabi,
      :trn_furikomibi,
      :trn_uriagebi,
      :trn_uriageno,
      :trn_uriageeda,
      :trn_biko1,
      :trn_biko2,
      :trn_biko3,
      :trn_maxeda,
      :trn_yuko,
      :image
      )
  end


end
