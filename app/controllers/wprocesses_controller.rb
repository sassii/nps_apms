class WprocessesController < ApplicationController
  def index
        # ユーザーからの入力がない場合のデフォルト検索条件を設定
        default_params = { gaityucd: '001', actual_end_date_presence: 'blank' }

        # paramsにデフォルト値をマージする。ユーザーからの入力がある場合はそれを優先する。
        search_params = params.permit(:gaityucd, :actual_end_date_presence, :employee_name).reverse_merge(default_params)
    
        base_query = Wprocess.all.includes(:work_items)
    
        if search_params[:gaityucd].present?
          base_query = base_query.where("gaityucd LIKE ?", "%#{search_params[:gaityucd]}%")
        end
        
        if search_params[:employee_name].present?
          base_query = base_query.joins(work_items: :employee).where('employees.employee_name LIKE ?', "%#{search_params[:employee_name]}%")
        end
    
        case search_params[:actual_end_date_presence]
        when 'present'
          # actual_end_dateが設定されているwork_itemsがあるWprocessを抽出
          base_query = base_query.joins(:work_items).where.not(work_items: { actual_end_date: nil }).distinct
        when 'blank'
          # actual_end_dateが設定されていない、またはwork_itemsが登録されていないWprocessを抽出
          base_query = base_query.left_joins(:work_items).where("work_items.id IS NULL OR work_items.actual_end_date IS NULL").distinct
        end
    
        @wprocesses = base_query(:deadline)

    # @wprocesses = Wprocess.includes(:work_items, t_order: :kokyaku).all

    
  end

  def index2
    # ユーザーからの入力がない場合のデフォルト検索条件を設定
    default_params = { gaityucd: '001', actual_end_date_presence: 'blank' }

    # paramsにデフォルト値をマージする。ユーザーからの入力がある場合はそれを優先する。
    search_params = params.permit(:gaityucd, :actual_end_date_presence, :employee_name).reverse_merge(default_params)

    base_query = Wprocess.all.includes(:work_items)

    if search_params[:gaityucd].present?
      base_query = base_query.where("gaityucd LIKE ?", "%#{search_params[:gaityucd]}%")
    end
    
    if search_params[:employee_name].present?
      base_query = base_query.joins(work_items: :employee).where('employees.employee_name LIKE ?', "%#{search_params[:employee_name]}%")
    end

    case search_params[:actual_end_date_presence]
    when 'present'
      # actual_end_dateが設定されているwork_itemsがあるWprocessを抽出
      base_query = base_query.joins(:work_items).where.not(work_items: { actual_end_date: nil }).distinct
    when 'blank'
      # actual_end_dateが設定されていない、またはwork_itemsが登録されていないWprocessを抽出
      base_query = base_query.left_joins(:work_items).where("work_items.id IS NULL OR work_items.actual_end_date IS NULL").distinct
    end

    @wprocesses = base_query(:deadline)
  end

  def index3
    # ユーザーからの入力がない場合のデフォルト検索条件を設定
    #default_params = { gaityucd: '001', actual_end_date_presence: 'blank' }
    default_params = { actual_end_date_presence: 'blank' }

    # paramsにデフォルト値をマージする。ユーザーからの入力がある場合はそれを優先する。
    search_params = params.permit(:gaityucd, :actual_end_date_presence, :employee_name).reverse_merge(default_params)

    base_query = Wprocess.all.includes(:work_items)

    if search_params[:gaityucd].present?
      base_query = base_query.where("gaityucd LIKE ?", "%#{search_params[:gaityucd]}%")
    end
    
    if search_params[:employee_name].present?
      base_query = base_query.joins(work_items: :employee).where('employees.employee_name LIKE ?', "%#{search_params[:employee_name]}%")
    end

    case search_params[:actual_end_date_presence]
    when 'present'
      # actual_end_dateが設定されている
      #base_query = base_query.joins(:work_items).where.not(work_items: { actual_end_date: nil }).distinct
      base_query = base_query.where.not(actual_end_date: nil )
    when 'blank'
      # actual_end_dateが設定されていない
      #base_query = base_query.left_joins(:work_items).where("work_items.id IS NULL OR work_items.actual_end_date IS NULL").distinct
      base_query = base_query.where(actual_end_date: nil)
    end

    @wprocesses = base_query.joins(:t_order).order('wprocesses.deadline', 't_orders.jyutyuno', 't_orders.jyutyueda').page(params[:page]).per(30)
  end

  def show
    @wprocess = Wprocess.includes(work_items: :employee).find(params[:id])
  end

  def update_data
    Wprocess.create_wprocesses_for_incomplete_t_orders
      # リダイレクト先をパラメータに基づいて決定する
    redirect_path = case params[:redirect_to]
    when 'index2'
      wprocesses_index2_path
    when 'index3'
      wprocesses_index3_path
    else
      # デフォルトのリダイレクト先
      root_path
    end

  end

  def complete
    @wprocess = Wprocess.find(params[:id])
    @wprocess.actual_end_date = Date.today

    if @wprocess.save
      redirect_to wprocesses_index3_path(params.permit(:gaityucd, :actual_end_date_presence, :employee_name)), notice: 'プロセスが完了しました。'
    else
      redirect_to wprocesses_index3_path(params.permit(:gaityucd, :actual_end_date_presence, :employee_name)), alert: 'プロセスの完了に失敗しました。'
    end

 #   @wprocesses = search_wprocesses(params).order(:deadline).page(params[:page]).per(30)
 #   render 'index3'

  end

  def search_wprocesses(search_params)
    #default_params = { gaityucd: '001', actual_end_date_presence: 'blank' }
    default_params = { actual_end_date_presence: 'blank' }

    # paramsにデフォルト値をマージする。ユーザーからの入力がある場合はそれを優先する。
    search_params = params.permit(:gaityucd, :actual_end_date_presence, :employee_name).reverse_merge(default_params)

    base_query = Wprocess.all.includes(:work_items)

    if search_params[:gaityucd].present?
      base_query = base_query.where("gaityucd LIKE ?", "%#{search_params[:gaityucd]}%")
    end
    
    if search_params[:employee_name].present?
      base_query = base_query.joins(work_items: :employee).where('employees.employee_name LIKE ?', "%#{search_params[:employee_name]}%")
    end

    case search_params[:actual_end_date_presence]
    when 'present'
      # actual_end_dateが設定されている
      #base_query = base_query.joins(:work_items).where.not(work_items: { actual_end_date: nil }).distinct
      base_query = base_query.where.not(actual_end_date: nil )
    when 'blank'
      # actual_end_dateが設定されていない
      #base_query = base_query.left_joins(:work_items).where("work_items.id IS NULL OR work_items.actual_end_date IS NULL").distinct
      base_query = base_query.where(actual_end_date: nil)
    end
  end



  
  before_action :set_beginning_of_week

  private
  def set_beginning_of_week
    Date.beginning_of_week = :sunday
  end

  # base_queryメソッドの定義
  def base_query(order_by)
    Wprocess.order(order_by)
  end
end
