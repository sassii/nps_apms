class WorkItemsController < ApplicationController
  before_action :set_wprocess, only: [:new, :create]

  protect_from_forgery :except => [:destroy]

  def new
    @work_items = @wprocess.work_items.new
  end

  def show
    @work_item = WorkItem.find(params[:id])
    @wprocess = @work_item.wprocess
    @work_items = @wprocess.work_items.includes(:employee, :machine)
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "WorkItemが見つかりません"
    redirect_to work_items_path
  end


  def create
    @work_items = @wprocess.work_items.new(work_item_params)
    if @work_items.save
      redirect_to wprocesses_url, notice: '作業工程が正常に登録されました。'
    else
      render :new
    end
  end

  def edit
    @wprocess = Wprocess.find(params[:wprocess_id])
    @work_item = @wprocess.work_items.find(params[:id])
  end

  def update
    @wprocess = Wprocess.find(params[:wprocess_id])
    @work_item = @wprocess.work_items.find(params[:id])
    if @work_item.update(work_item_params)
      redirect_to wprocesses_path, notice: 'WorkItemが正常に更新されました。'
    else
      render :edit
    end
  end

  def start
    @work_item = WorkItem.find(params[:id])
    @work_item.update(actual_start_date: Time.current)
    redirect_to work_items_path, notice: '作業が開始されました。'
  end

  def complete
    @work_item = WorkItem.find(params[:id])
    @work_item.actual_end_date = Date.today # システム日付を設定

    if @work_item.save
      redirect_to work_items_path, notice: 'Work item was successfully completed.'
    else
      redirect_to work_items_path, alert: 'Failed to complete the work item.'
    end
  end

  def delete_selected_work_items
    # 選択されたwork_item_idsを取得
    work_item_ids = params[:work_item_ids]
    
    # work_item_idsが存在する場合のみ削除処理を実行
    if work_item_ids.present?
      WorkItem.where(id: work_item_ids).destroy_all
      flash[:notice] = "選択された作業項目が削除されました。"
    else
      flash[:alert] = "削除する作業項目が選択されていません。"
    end
    
    redirect_to work_items_path
  end
  

  private

  def set_wprocess
    @wprocess = Wprocess.find(params[:wprocess_id])
  end

  def work_item_params
    params.require(:work_item).permit(:work_item_id, :wprocess_id, :work_item_name, :quantity, :employee_id, :machine_id, :planned_hours, :actual_hours, :actual_start_date, :actual_end_date, :actual_start_time, :actual_end_time)
  end
end
