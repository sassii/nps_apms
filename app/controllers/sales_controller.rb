class SalesController < ApplicationController
  def index
    selected_date = params[:date].present? ? Date.parse(params[:date]) : Date.today

    # 現在の月の初日
    # 開始日 = Date.today.beginning_of_month
    開始日 = selected_date.beginning_of_month

    # 現在の月の末日
    # 終了日 = Date.today.end_of_month
    終了日 = selected_date.end_of_month

    #今日 = Date.today
    今日 = selected_date
    期初日 = 今日.month > 5 ? Date.new(今日.year, 6, 1) : Date.new(今日.year - 1, 6, 1)

    期末日 = 今日.month > 5 ? Date.new(今日.year + 1, 5, 31) : Date.new(今日.year, 5, 31)


    #当月売上実績
    @m_uriage = TOrder.select('SUM(t_orders.kin) as total_amount')
                      .where('yuko_kbn = 0 AND t_orders.uriagebi BETWEEN ? AND ?', 開始日, 終了日)

    @d_uriage = TOrder.select('SUM(t_orders.kin) as total_amount')
                      .where('yuko_kbn = 0 AND t_orders.uriagebi = ?', 今日)

    @d_uriage_m = TOrder.all.where('yuko_kbn = 0 AND t_orders.uriagebi = ?' , 今日).order('jyutyuno, jyutyueda')
  
                                          

    #当月売上予定
    @t_uriageyo = TOrder.select('SUM(t_orders.kin) as total_amount')
    .where('yuko_kbn = 0 AND t_orders.noki BETWEEN ? AND ?', 開始日, 終了日)    

  end
end
