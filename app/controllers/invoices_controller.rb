class InvoicesController < ApplicationController
  def send_selected
    if params[:commit_create]
      if params[:target_month].present?
        target_month_d = params[:target_month]

      # 年と月を取得し、6桁の数字に変換
        target_month = Date.parse(params[:target_month])
        formatted_month = target_month.strftime("%Y%m") # "YYYYMM"形式に変換
    
        begin
          ActiveRecord::Base.transaction do
            kokyakus = Kokyaku.where.not(gyosyu: [nil, ''])
            
            kokyakus.each do |kokyaku|
              pdf_filename = "#{kokyaku.id.to_s.rjust(3, '0')}#{formatted_month}請求書印.pdf"
              pdf_path = "c:\\apms_data\\請求書\\#{pdf_filename}"
              pdf_filename2= "#{kokyaku.id.to_s.rjust(3, '0')}#{formatted_month}.pdf"

              # ファイルが存在するか確認
              if File.exist?(pdf_path)
                #destination_path = Rails.root.join('public', 'uploads', 'invoices', pdf_filename)
                destination_path = Rails.root.join('public', 'uploads', 'invoices', pdf_filename2)
                FileUtils.cp(pdf_path, destination_path)
                
                pdf_url = "uploads/invoices/#{File.basename(destination_path)}"
                
                # 同一年月同一顧客のInvoiceが既に存在するか確認
                existing_invoice = Invoice.find_by(kokyaku_id: kokyaku.id, issued_date: Date.today.beginning_of_month..Date.today.end_of_month)
                
                unless existing_invoice
                  Invoice.create!(
                    kokyaku_id: kokyaku.id,
                    issued_date: target_month_d,
                    pdf_path: pdf_url,
                    pdf_url: pdf_url
                  )
                end
              else
                Rails.logger.info "File not found: #{pdf_path} for Kokyaku ID: #{kokyaku.id}"
              end
            end
          end
      
          @invoices = Invoice.where(issued_date: Date.today.beginning_of_month..Date.today.end_of_month)
          render :index
        rescue ActiveRecord::RecordInvalid => e
          logger.error "Invoice creation failed: #{e.message}"
          @invoices = Invoice.where(issued_date: Date.today)
          flash[:alert] = "請求書の作成に失敗しました。"
          render :index
        rescue => e
          logger.error "Error: #{e.message}"
          logger.error e.backtrace.join("\n")
          @invoices = Invoice.where(issued_date: Date.today)
          flash[:alert] = "予期しないエラーが発生しました。"
          render :index
        end
      else
        flash.now[:alert] = '対象年月を指定してください。'
        render :new
      end
  
    elsif params[:commit_delete]
      if params[:invoice_ids].present?
        Invoice.where(id: params[:invoice_ids]).destroy_all
        flash[:notice] = "選択された請求書を削除しました。"
      else
        flash[:alert] = "削除する請求書を選択してください。"
      end
      redirect_to invoices_path
  
    elsif params[:commit_send]
      email_subject = params[:email_subject]
      email_body = params[:email_body]
      invoice_ids = params[:invoice_ids]

      if invoice_ids.present?
        invoices = Invoice.where(id: invoice_ids)

        invoices.each do |invoice|
          Rails.logger.info "Sending email to: #{invoice.kokyaku.gyosyu}, PDF Path: #{invoice.pdf_path}"
          # バックグラウンドジョブを呼び出す
          SendInvoiceEmailJob.perform_later(invoice.id, email_subject, email_body)
          
        # メール送信後に issued_date を更新
          invoice.update(sent_at: Date.today) # ここで issued_date を更新
        end

        flash[:notice] = "選択された請求書を送信しました。"
      else
        flash[:alert] = "請求書を選択してください。"
      end
    else
      redirect_to invoices_path
    end
  end
  
  

  private

 
  def index
    if params[:target_month].present?
      # 年と月を取得し、6桁の数字に変換
      target_month = Date.parse(params[:target_month])
      formatted_month = target_month.strftime("%Y%m") # "YYYYMM"形式に変換

      # フィルタリング処理
      @invoices = Invoice.where(issued_date: Date.today.beginning_of_month..Date.today.end_of_month)
    else
      #@invoices = Invoice.all
    end
  end



  def invoice_params
    params.require(:invoice).permit(:sent_at)
  end

end
