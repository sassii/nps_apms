class GaityusController < ApplicationController
  before_action :set_gaityu, only: %i[ show edit update destroy ]

  # GET /gaityus or /gaityus.json
  def index
    @gaityus = Gaityu.all
  end

  # GET /gaityus/1 or /gaityus/1.json
  def show
  end

  # GET /gaityus/new
  def new
    @gaityu = Gaityu.new
  end

  # GET /gaityus/1/edit
  def edit
  end

  # POST /gaityus or /gaityus.json
  def create
    @gaityu = Gaityu.new(gaityu_params)

    respond_to do |format|
      if @gaityu.save
        format.html { redirect_to @gaityu, notice: "Gaityu was successfully created." }
        format.json { render :show, status: :created, location: @gaityu }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @gaityu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gaityus/1 or /gaityus/1.json
  def update
    respond_to do |format|
      if @gaityu.update(gaityu_params)
        format.html { redirect_to @gaityu, notice: "Gaityu was successfully updated." }
        format.json { render :show, status: :ok, location: @gaityu }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @gaityu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gaityus/1 or /gaityus/1.json
  def destroy
    @gaityu.destroy
    respond_to do |format|
      format.html { redirect_to gaityus_url, notice: "Gaityu was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gaityu
      @gaityu = Gaityu.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def gaityu_params
      params.require(:gaityu).permit(:gai_cd, :gai_name, :gai_kana, :gai_ryaku, :gai_naiyo, :yuko_kbn)
    end
end
