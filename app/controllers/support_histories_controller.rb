class SupportHistoriesController < ApplicationController
  def index
    @support_history = SupportHistory.all
  end

  def show
    @support_history = SupportHistory.find(params[:id])  
  end

  def new
    @support_history = SupportHistory.new
  end

  def create
    @support_history = SupportHistory.new(support_history_params)
    @support_history.save
    redirect_to support_history_path(@support_history)
  end

  def edit
    @support_history = SupportHistory.find(params[:id])
  end

  def update
    @support_history = SupportHistory.find(params[:id])
    @support_history.update(support_history_params)
    redirect_to support_history_path(@support_history)
  end

  private

  def support_history_params
    params.require(:support_history).permit(:ulist_id, :s_date, :s_content, :s_tanto)
  end
end
