class UlistsController < ApplicationController
  def index
    @q = Ulist.ransack(params[:q])
    @ulists = @q.result.page params[:page]
#    @ulists = @q.result
#    @Ulist = Ulist.all.page(params[:page]).per(10)
  end

  def search
    @q = Ulist.ransack(search_params)
    @ulists = @q.result
  end

  def show
    @ulist = Ulist.find(params[:id])
    @support_history = SupportHistory.new
    @support_history = SupportHistory.where('ulist_id = ?'  , params[:id])
  end

  def edit
    @ulist = Ulist.find(params[:id])
    @ulist.support_histories.build
  end

  def update
    @ulist = Ulist.find(params[:id])
    if @ulist.update(update_ulist_params)
      redirect_to ulist_path(@ulist)
    else
      render :edit
    end
  end

  def new
    @ulist = Ulist.new
  end

  def create
    if request.post? then
      @ulist = Ulist.create(ulist_params)        
      
      if @ulist.save then
        redirect_to '/'
      else
        render 'new'
      end
    end
  end 

  private
  def ulist_params
    params.require(:ulist).permit(:name ,
          :director,
          :univ,
          :faculty,
          :department,
          :laboratory,
          :zipcode,
          :address,
          :tel,
          :email_add,
          :order_amount,
          :order_times,
          :order_last,
          :estimate_last)
  end

  def update_ulist_params
    params.require(:ulist).permit(:name ,
          :director,
          :univ,
          :faculty,
          :department,
          :laboratory,
          :zipcode,
          :address,
          :tel,
          :email_add,
          :order_amount,
          :order_times,
          :order_last,
          :estimate_last,
          support_histories_attributes: [:id, :s_date, :s_content, :s_tanto, :_destroy])
  end

  def search_params
    params.require(:q).permit!
  end

end
