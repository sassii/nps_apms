class WorkProgressesController < ApplicationController
  before_action :set_wprocess, only: [:new, :create]

  def new
    @work_progress = @wprocess.work_progresses.new
  end

  def create
    @work_progress = @wprocess.work_progresses.new(work_progress_params)
    if @work_progress.save
      redirect_to @wprocess, notice: '作業進捗が正常に登録されました。'
    else
      render :new
    end
  end

  private

  def set_wprocess
    @wprocess = Wprocess.find(params[:wprocess_id])
  end

  def work_progress_params
    params.require(:work_progress).permit(:field1, :field2) # 実際のフィールド名に置き換えてください
  end
end
