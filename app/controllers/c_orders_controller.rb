class COrdersController < ApplicationController
  #require 'csv' 
  require 'roo'

  def new
    @d_jyutyu = [] # 空の配列で初期化
    # CSVファイルアップロード用のフォームを表示
  end

  def create
    # Excelファイルを受け取る
    excel_file = params[:file]

    # Excelをパースして内容を表示
    @orders = []

    begin
      # Rooを使ってExcelファイルを読み込む
      spreadsheet = Roo::Spreadsheet.open(excel_file.path)

      # シートを指定（最初のシートを使用）
      spreadsheet.default_sheet = spreadsheet.sheets.first

      # ヘッダーを取得
      header = spreadsheet.row(1)

      # データ行を取得
      (2..spreadsheet.last_row).each do |i|
        row_data = Hash[[header, spreadsheet.row(i)].transpose]
        @orders << row_data
      end

      ActiveRecord::Base.transaction do

        # nameが「受注」のレコードを取得
        number_record = Number.lock.find_by(name: '受注')

        if number_record
          # currentの値を取得してインクリメント
          current_number = number_record.current + 1
          number_record.update(current: current_number, updated_at: '2023/12/09') # currentを更新
          
          #number_record.save

          # typeの値を取得
          order_type = number_record.type

          # 新しい受注番号を生成（"000000"形式）
          new_order_number = "#{order_type}#{format('%04d', current_number)}"

          # 各行に受注番号を付与
          @orders.each do |order|
            order['受注番号'] = new_order_number
          end

        else
          flash[:error] = "受注番号の管理レコードが見つかりません。"
          render :new and return
        end

      end


    rescue => e
      Rails.logger.error("エラー: #{e.message}")
      flash[:error] = "Excelの解析中にエラーが発生しました。"
      render :new and return
    end

    # デバッグ用
    Rails.logger.debug(@orders.inspect) # コンソールに出力

    # 表示用のビューファイルに渡す
    render :show_orders
  end

  def import
    # 受注データを取得
    order_numbers = params[:selected_orders] || []      # 受注番号の配列を取得
    eda_values = params[:eda] || []                      # 枝番の配列を取得
    zuban_values = params[:zuban] || []                  # 図番の配列を取得
    hinmei_values = params[:hinmei] || []                # 品名の配列を取得
    suryo_values = params[:suryo] || []                  # 数量の配列を取得
    tanka_values = params[:tanka] || []                  # 単価の配列を取得
    kin_values = params[:kin] || []                      # 金額の配列を取得
    noki_values = params[:noki] || []                    # 納期の配列を取得
    biko_values = params[:biko] || []                    # 備考の配列を取得
  
    Rails.logger.debug("受注データの読み込み完了") # 読み込んだデータのログ
  
    # 配列の長さを確認
    length = [order_numbers.length, eda_values.length, zuban_values.length, hinmei_values.length, 
              suryo_values.length, tanka_values.length, kin_values.length, noki_values.length, 
              biko_values.length].uniq
  
    if length.length > 1
      Rails.logger.error("配列の長さが一致しません: #{length}")
      return
    end

    # チェックボックスの状態を確認
    history_priority = params[:history_priority] == '1'
  
    # 読み込まれたデータをすべて登録
    order_numbers.each_with_index do |order_identifier, idx|
      Rails.logger.debug("現在処理中の受注番号: #{order_identifier}") # 現在の受注番号のログ
  
      # 各行のデータを取得
      order_data = {
        jyutyuno: order_identifier,                  # 受注番号
        jyutyueda: eda_values[idx],                  # 枝番
        zuban: zuban_values[idx],                    # 図番
        hinmei: hinmei_values[idx],                  # 品名
        suryo: suryo_values[idx],                    # 数量
        sakuseisu: suryo_values[idx],                # 追加の数量
        tanka: tanka_values[idx],                    # 単価
        kin: kin_values[idx],                        # 金額
        noki: noki_values[idx],                      # 納期
        biko: biko_values[idx]                       # 備考
      }

      # 過去の実績データを検索
      past_orders = TOrder.where(zuban: order_data[:zuban], yuko_kbn: 0).order(id: :desc).first

      # 新しいT_Orderインスタンスを作成
      new_order = TOrder.new(order_data)
  
      # コンスタント値を設定
      new_order.jyutyukbn = 0
      new_order.tyokusokbn = 0
      new_order.jyutyubi = Date.today
      new_order.kokyaku_id = '273' 
      new_order.tanto_id = '004'
      new_order.tanka = order_data[:tanka]      
      new_order.kin = order_data[:tanka].to_i * order_data[:suryo].to_i
      new_order.zei = 0
      new_order.zairyohi = 0
      new_order.programhi = 0
      new_order.status = 1

      # 過去の実績データが存在する場合、gaityusuryo1～10を設定
      if past_orders
        Rails.logger.debug("history_priority: #{params[:history_priority] }")
        if history_priority 
          Rails.logger.debug("過去の単価: #{past_orders.tanka}")
          new_order.tanka = past_orders.tanka || 0  # 履歴の単価を設定、履歴がない場合は0
          new_order.kin = order_data[:suryo].to_i * new_order.tanka  # 金額を計算
        end

        Rails.logger.debug("past_orders.noki: #{past_orders.noki}")
        past_noki = past_orders.noki # これは Date オブジェクト
      
        # 現在の納期をDateオブジェクトに変換
        current_noki = Date.parse(order_data[:noki]) rescue nil

        past_suryo = past_orders.suryo.to_f
        new_suryo = order_data[:suryo].to_f
      
        (1..10).each do |i|
            # gaityucdがnullでない場合のみ、他のフィールドを設定
          if past_orders.send("gaityucd#{i}").present?
            new_order.send("gaityucd#{i}=", past_orders.send("gaityucd#{i}"))
            new_order.send("gaityunaiyo#{i}=", past_orders.send("gaityunaiyo#{i}"))
            # new_order.send("gaityusuryo#{i}=", past_orders.send("gaityusuryo#{i}"))
            new_order.send("gaityutanka#{i}=", past_orders.send("gaityutanka#{i}"))

            # 過去の gaityusuryo を取得
            past_gaityusuryo = past_orders.send("gaityusuryo#{i}").to_f

            # gaityusuryo の計算
            if past_suryo > 0
              calculated_gaityusuryo = (past_gaityusuryo * new_suryo) / past_suryo
              # 小数点以下を切り上げ
              new_gaityusuryo = calculated_gaityusuryo.ceil
        
              # 割り切れない場合のメッセージ表示
              if calculated_gaityusuryo % 1 != 0
                # 画面上にメッセージを表示（例：モデルの errors に追加）
                new_order.errors.add(:base, "gaityusuryo#{i} が割り切れないため、小数点以下を切り上げました。")
              end
            else
              # 過去の suryo が0の場合、gaityusuryo を過去の値で設定
              new_gaityusuryo = past_gaityusuryo
            end
        
            # 新しい gaityusuryo を設定
            new_order.send("gaityusuryo#{i}=", new_gaityusuryo)


          
            # 差分を計算
            if past_noki.present? && past_orders.send("gaityunoki#{i}").present?
              Rails.logger.debug("Both past_noki and gaityunoki#{i} are present.")
              
              # past_orders.gaityunokiX の型を確認
              if past_orders.send("gaityunoki#{i}").is_a?(String)
                gaityunoki_date = Date.parse(past_orders.send("gaityunoki#{i}")) rescue nil
              elsif past_orders.send("gaityunoki#{i}").is_a?(Date)
                gaityunoki_date = past_orders.send("gaityunoki#{i}")
              else
                gaityunoki_date = nil
              end
          
              if gaityunoki_date.present?
                date_difference = (past_noki - gaityunoki_date).to_i
                new_order.send("gaityunoki#{i}=", current_noki - date_difference)
              else
                new_order.send("gaityunoki#{i}=", current_noki)
              end
            else
              new_order.send("gaityunoki#{i}=", current_noki)
            end
          else
                # gaityucdがnullの場合、gaityunaiyo、gaityusuryo、gaityutanka、gaityunokiをnullに設定
            new_order.send("gaityunaiyo#{i}=", nil)
            new_order.send("gaityusuryo#{i}=", 0)
            new_order.send("gaityutanka#{i}=", 0)
            new_order.send("gaityunoki#{i}=", nil)
          end

        end

        new_order.ryakusyo = past_orders.ryakusyo
        new_order.syoken = past_orders.syoken
        new_order.seizokbn = past_orders.seizokbn
      else
        if history_priority 
          new_order.tanka = 0  # 履歴の単価を設定、履歴がない場合は0
          new_order.kin = 0
        end

        new_order.gaityucd1 = '000'
        new_order.gaityunaiyo1 = '全加工'
        new_order.gaityusuryo1 = order_data[:suryo].to_i
        new_order.gaityutanka1 = 0
        new_order.gaityunoki1 = order_data[:noki]
        new_order.ryakusyo = 'NPS'
        new_order.seizokbn = 0
      end

      # 'noki' を Date オブジェクトに変換してセット
      new_order.noki = Date.parse(order_data[:noki]) rescue nil
      new_order.yuko_kbn = 0

      Rails.logger.debug("max_branch_number: #{params[:max_branch_number]}")
      
      new_order.maxeda = params[:max_branch_number]

      new_order.created_at = Date.today
      new_order.updated_at = Date.today

      # インスタンスを保存
      if new_order.save
        Rails.logger.info("新規受注データが登録されました: #{new_order.id}")
      else
        Rails.logger.error("受注データの登録に失敗しました: #{new_order.errors.full_messages.join(', ')}")
      end
    end

    redirect_to orders_path, notice: '受注データの登録が完了しました。'
  end

  
  
end
