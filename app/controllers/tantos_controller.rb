class TantosController < ApplicationController
  before_action :set_tanto, only: %i[ show edit update destroy ]

  # GET /tantos or /tantos.json
  def index
    @tantos = Tanto.all
  end

  # GET /tantos/1 or /tantos/1.json
  def show
  end

  # GET /tantos/new
  def new
    @tanto = Tanto.new
  end

  # GET /tantos/1/edit
  def edit
  end

  # POST /tantos or /tantos.json
  def create
    @tanto = Tanto.new(tanto_params)

    respond_to do |format|
      if @tanto.save
        format.html { redirect_to @tanto, notice: "Tanto was successfully created." }
        format.json { render :show, status: :created, location: @tanto }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tanto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tantos/1 or /tantos/1.json
  def update
    respond_to do |format|
      if @tanto.update(tanto_params)
        format.html { redirect_to @tanto, notice: "Tanto was successfully updated." }
        format.json { render :show, status: :ok, location: @tanto }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tanto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tantos/1 or /tantos/1.json
  def destroy
    @tanto.destroy
    respond_to do |format|
      format.html { redirect_to tantos_url, notice: "Tanto was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tanto
      @tanto = Tanto.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tanto_params
      params.require(:tanto).permit(:tanto_cd, :tanto_name)
    end
end
