class OrdersController < ApplicationController
  def index
    #selected_date = params[:date].present? ? Date.parse(params[:date]) : Date.today
    # 選択された年月を取得
    selected_date = params[:date].present? ? Date.parse("#{params[:date]}-01") : Date.today
    # 現在の月の初日
    # 開始日 = Date.today.beginning_of_month
    開始日 = selected_date.beginning_of_month

    # 現在の月の末日
    # 終了日 = Date.today.end_of_month
    終了日 = selected_date.end_of_month

    #今日 = Date.today
    今日 = selected_date
    期初日 = 今日.month > 5 ? Date.new(今日.year, 6, 1) : Date.new(今日.year - 1, 6, 1)

    期末日 = 今日.month > 5 ? Date.new(今日.year + 1, 5, 31) : Date.new(今日.year, 5, 31)

    #当月受注実績
    @m_jyutyu = TOrder.select('SUM(t_orders.kin) as total_amount')
                      .where('yuko_kbn = 0 AND t_orders.jyutyubi BETWEEN ? AND ?', 開始日, 終了日)

    #当日受注実績
    #@d_jyutyu = TOrder.select('SUM(t_orders.kin) as total_amount')
    #                  .where('yuko_kbn = 0 AND t_orders.jyutyubi = ?' , 今日)

    #当日受注明細
    @d_jyutyu_m = TOrder.all.where('yuko_kbn = 0 AND t_orders.jyutyubi BETWEEN ? AND ?', 開始日, 終了日).order('id DESC')

    #@d_jyutyu_n = TOrder.all.where('yuko_kbn = 0 AND t_orders.jyutyubi = ?' , 今日).order('noki: params[:direction] || :asc')

  end

  def sort_by_noki
    selected_date = params[:date].present? ? Date.parse(params[:date]) : Date.today
    @m_jyutyu = TOrder.where('yuko_kbn = ? AND jyutyubi = ?', 0, selected_date)
                         .order(noki: params[:direction] || :asc)
    render :index
  end

  def detail
    @order = TOrder.find(params[:id]) # IDに基づいてレコードを取得
    if @order.nil?
      redirect_to orders_path, alert: '受注が見つかりませんでした。'
    end
  end

end
