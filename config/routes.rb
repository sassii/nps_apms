Rails.application.routes.draw do
  get 'sales/index'
  get 'orders/index'
  get 'work_items/new'
  get 'work_items/index'
  get 'work_progresses/new'
  get 'work_progresses/index'
  get 'work_progress/new'
  get 'work_progress/index'
  get 'work_processes/new'
  get 'work_processes/index'
  resources :machines
  resources :employees
  resources :work_statuses

  root to: 't_order#index'
  
  get 't_order/index'
  get 't_order/index_a'
  get 't_order/index_b'
  

  get 'order_trns/index'
  get 'order_trns/index_image'
  
  get 'order_trns/show'
  patch 'order_trns/:id/edit', to: 'order_trns#edit' 

  get 'order_headers/index'
  get 'order_headers/show'

  #get 'top/index'
  #root to: 'top#index'

  resources :tokuis
  resources :gaityus
  resources :tantos
#  devise_for :users
#  get 'home/index'
#  get 'home/show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
#  root to: "ulists#index"


  

  devise_for :users, :controllers => {
    :registrations => 'users/registrations',
    :sessions => 'users/sessions'   
  } 

  devise_scope :user do
    get "sign_in", :to => "users/sessions#new"
    get "sign_out", :to => "users/sessions#destroy" 
  end

  get 'users/index' 
  resources :users
  resources :ulists
  resources :support_histories

  #get 'orderheaders/index'
  resources :order_headers
  resources :order_trns
  
  get 'search', to: 'ulists#search'



  resources :employees
  resources :machines
  resources :work_statuses

  get 'wprocesses/index2', to: 'wprocesses#index2', as: 'wprocesses_index2'
  get 'wprocesses/index3', to: 'wprocesses#index3', as: 'wprocesses_index3'
  get 'wprocesses/index', to: 'wprocesses#index', as: 'wprocesses_index'
  get 'wprocesses' => 'wprocesses#index2'

  resources :wprocesses, except: :index do
    get 'index2', on: :collection
    resources :work_items
    collection do
      patch 'update_data', to: 'wprocesses#update_data'
    end
  end
  
  resources :wprocesses do
    member do
      patch :complete
    end
  end

  resources :wprocesses do
    resources :work_items
  end
  
  resources :work_items do
    member do
      patch :start
      patch :complete
    end
  end

#  delete 'delete_selected_work_items', to: 'work_items#delete_selected_work_items', as: 'delete_selected_work_items'

  resources :work_items do
    collection do
      delete :delete_selected
    end
  end

  get 'orders', to: 'orders#index', as: 'orders'
  
  resources :orders do
    collection do
      get 'sort_by_noki'
    end
    member do
      get 'detail', to: 'orders#detail', as: 'order_detail' # 受注詳細のルート
    end
  end
  
  get 'sales', to: 'sales#index', as: 'sales'

  resources :invoices do
    collection do
      post :create_from_kokyaku
    end
  end

  # POSTリクエスト用のルート
  post 'delete_selected_invoices', to: 'invoices#delete_selected', as: 'delete_selected_invoices'

  # DELETEリクエスト用のルート
  delete 'delete_selected_invoices', to: 'invoices#delete_selected'

  post 'send_selected_invoices', to: 'invoices#send_selected'

  resources :c_orders do
    collection do
      post :import
    end
  end
end
